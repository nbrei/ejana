#ifndef _EICGEN_FIT_H_
#define _EICGEN_FIT_H_

#include <vector>
using namespace std;

// Genfitter
#include <MeasurementCreator.h>
#include <GFRaveVertexFactory.h>
#include <AbsKalmanFitter.h>
#include <EventDisplay.h>

#include <TVector3.h>

#include <math.h>

// namespace genfit {
// 	class MeasurementCreator;
// 	class GFRaveVertexFactory;
// 	class AbsKalmanFitter;
// 	class EventDisplay;
// }
class DMagneticFieldMap;

typedef struct{
	float x,y,z;		///< point in lab coordinates
	float phi_circle;	///< phi angle relative to axis of helix
}trkHit_t;


class eicGenFit{
	public:
		eicGenFit(void);
		~eicGenFit();
		void FitTrack(void);
		void AddHitXYZ(float x, float y, float z);
		void Clear();

	protected:
		vector<trkHit_t*> hits;
		 
		// init MeasurementCreator
  		genfit::MeasurementCreator measurementCreator;  

  		// init event display
  		genfit::EventDisplay* display = genfit::EventDisplay::getInstance();
 
 		// init fitter
 		genfit::AbsKalmanFitter* fitter;

  		// init vertex factory
  		//genfit::GFRaveVertexFactory vertexFactory;
};



#endif //_EICGEN_FIT_H_
