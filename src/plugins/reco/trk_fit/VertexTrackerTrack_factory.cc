#include <iostream>
#include <iomanip>
#include <fmt/core.h>

#include <JANA/JEvent.h>

#include "VertexTrackerTrack_factory.h"
#include "VertexTrackerCluster.h"

using namespace std;


void VertexTrackerTrack_factory::Process(const std::shared_ptr<const JEvent>& event)
{
    using namespace fmt;

    auto clusters = event->Get<VertexTrackerCluster>();

    // Print a new event
    if(print_debug_info) {
        print("\n\nEvent #{} tracks={}\n",event->GetEventNumber(), clusters.size());
    }

    std::vector<VertexTrackerTrack*> tracks;

    // Loop over clusters
    for(auto cluster : clusters){

        if(print_debug_info) {
            print("   new track, hits={} : (x, y, z)\n", cluster->hits.size());
        }


        // Need minimum of 6 hits to do fit
        if( cluster->hits.size() < 5 ) continue;

        // Do a fast helical fit to hits
        gen_fitter.Clear();
        for(auto h : cluster->hits) {
            if(print_debug_info) {
                print("       {:<10} {:<10} {:<10}\n", h->Xpos, h->Ypos, h->Zpos);
            }
            gen_fitter.AddHitXYZ( h->Xpos, h->Ypos, h->Zpos );
        }
        gen_fitter.FitTrack();
        //if( !isfinite(gen_fitter.p)     ) continue;
        //if( !isfinite(gen_fitter.theta) ) continue;
        //if( !isfinite(gen_fitter.phi)   ) continue;
        //if( !isfinite(gen_fitter.chisq) ) continue;

        auto trk = new VertexTrackerTrack;
        //trk->p     = gen_fitter.p;
        //trk->theta = gen_fitter.theta;
        //trk->phi   = gen_fitter.phi;
        //trk->q     = gen_fitter.q;
        //trk->chisq = gen_fitter.chisq;
        //trk->ndf   = (double)gen_fitter.GetNhits();

        tracks.push_back(trk);
    }
    event->Insert(tracks);
}


