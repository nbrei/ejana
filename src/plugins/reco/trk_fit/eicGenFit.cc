#include <iostream>
#include <algorithm>
#include <cmath>

#include "eicGenFit.h"

//---------- ROOT includes-------------------------

#include <TROOT.h>
#include <TClonesArray.h>
#include <TFile.h>
#include <TTree.h>
#include <TDatabasePDG.h>
#include <TMath.h>
#include <TGeoManager.h>
#include <TRandom.h>

// ------------- GENFIT Headers -------------------------
#include <ConstField.h>
#include <Exception.h>
#include <FieldManager.h>
#include <KalmanFitterRefTrack.h>
#include <StateOnPlane.h>
#include <Track.h>
#include <TrackPoint.h>

#include <MaterialEffects.h>
#include <RKTrackRep.h>
#include <TGeoMaterialInterface.h>
#include <EventDisplay.h>

#include <HelixTrackModel.h>
#include <MeasurementCreator.h>
#include <SpacepointMeasurement.h>

#include <DAF.h>

#include <GFRaveVertexFactory.h>

using namespace std;

//  double charge=1;
double VTX_y_res=0.01; //--- units ???
double VTX_t_res=0.01; //--- units ???

//-----------------
// eicGenFit (Constructor)
//-----------------
eicGenFit::eicGenFit()
{
    printf("eicGenFit: Hallo World!\n");

    new TGeoManager("Geometry", "DET geometry");
    TGeoManager::Import("geometryJLEIC.root");

    gRandom->SetSeed(14);

    // init geometry and mag. field
    genfit::FieldManager::getInstance()->init(new genfit::ConstField(0., 0., 30.)); // 15 kGauss / 1.5T
    gGeoManager->DefaultColors();

    genfit::MaterialEffects::getInstance()->init(new genfit::TGeoMaterialInterface());
    genfit::MaterialEffects::getInstance()->setNoEffects();

    // init event display
    //display = genfit::EventDisplay::getInstance();

    // init fitter
    fitter = new genfit::DAF(true);

    // init vertex factory
    //vertexFactory=genfit::GFRaveVertexFactory(2);
    //vertexFactory.setMethod("kalman-smoothing:1");
}

//-----------------
// FitTrack
//-----------------
void eicGenFit::FitTrack(void)
{
    
    using namespace genfit;
    static int iTrack=0;

    printf("    eicGenFit::Fit Track\n");
    // helix track model

    // Do we have hits at all?
    if(hits.empty()) {
        printf("    (!) hits.size()<=0");
        return;
    }
    
    // Set initial params

    TVector3 pos(hits[0]->x, hits[0]->y, hits[0]->z);
    TVector3 mom(hits[0]->x*1e1, hits[0]->y*1e1, hits[0]->z*1e1);
    const int pdg = 211;               // particle pdg code  13=mu
    const double charge = TDatabasePDG::Instance()->GetParticle(pdg)->Charge()/(3.);
    genfit::HelixTrackModel* helix = new genfit::HelixTrackModel(pos, mom, charge);
    measurementCreator.setTrackModel(helix);

    printf("   MY-01 \n");	  
 
    // trackrep
    genfit::AbsTrackRep* rep = new genfit::RKTrackRep(pdg);

    //----- eic -----
    TMatrixDSym covM(6);
    double resolution = VTX_y_res;
    for (int i = 0; i < 3; ++i) {
	    covM(i,i) = resolution*resolution;
    }

    printf("   MY-01 \n");	  
    for (int i = 3; i < 6; ++i) {
        // due to unknown initial momentum we give it bigger error
        covM(i,i) = pow(resolution / 2. / sqrt(3), 2); 
    }

    printf("   MY-02 \n");	  
    genfit::MeasuredStateOnPlane stateRef(rep);

    printf("   MY-03 \n");
    stateRef.setPosMomCov(pos, mom, covM);

    printf("   MY-04 \n");
    const genfit::StateOnPlane stateRefOrig(stateRef);

    printf("   MY-05 \n");	  

    //----------
    // create track
    TVectorD seedState(6);
    TMatrixDSym seedCov(6);
    
    rep->get6DStateCov(stateRef, seedState, seedCov);

    auto trackPtr = new genfit::Track(rep, seedState, seedCov);    

    // =================

    // create smeared measurements and add to track
    std::vector<genfit::AbsMeasurement*> measurement_S(2);
    TVectorD hitCoords_S(3);
    TMatrixDSym hitCov_S(3);
    hitCov_S*=0.;
    int detId=1;
    int measurementCounter=0;	
    int pointn=0; // number of points used in the fit
    int ihp=0;
    
    //----------
    try {
        // Put hits...
        for (uint ihit=0; ihit<hits.size(); ihit++) {
            auto hit = hits[ihit];
            hitCoords_S(0) = hit->x;
	        hitCoords_S(1) = hit->y;
	        hitCoords_S(2) = hit->z;
	  
	        hitCov_S(0,0) = VTX_y_res * VTX_y_res;
	        hitCov_S(1,1) = VTX_y_res * VTX_y_res;
	        hitCov_S(2,2) = VTX_t_res * VTX_t_res;

	        
	        measurementCounter++;
	        trackPtr->insertPoint(
                new genfit::TrackPoint(
                    new genfit::SpacepointMeasurement(hitCoords_S, hitCov_S, detId, ++ihp, NULL),
                    trackPtr));
	        pointn++;
        }
	    printf("pointn=%d \n",pointn);
    }
    catch(genfit::Exception& e) {
        std::cerr<<"Exception, next track"<<std::endl;
        std::cerr << e.what();
    }


    //check
    trackPtr->checkConsistency();
    
    // do the fit
    printf("--> do the fit!!!\n");
    try {
        fitter->processTrack(trackPtr);
    }
    catch(genfit::Exception& e) {
        std::cerr << e.what();
        std::cerr << "Exception, during fitting track" << std::endl;
    }
    

    printf("--> check fit\n");
    
    //check
    trackPtr->checkConsistency();

    cout<<" print fitTRack results "<<endl;
    trackPtr->Print();
    if (trackPtr->getFitStatus()->isFitted() && trackPtr->getFitStatus(rep)->isFitConverged()) {

        const genfit::MeasuredStateOnPlane& state = trackPtr->getFittedState();

        cout<<" MeasuredStateOnPlane  done "<<endl;

        TVector3 pos3 = state.getPos();
        TVector3 mom3 = state.getMom();


        double mrec =  mom3.Mag();
        //double prec =  pos3.Mag();
        //double theta=  mom3.Theta();
        double eta = -log(tan(mom3.Theta()*0.5));

        double trkmom = 999;

        double trkChi2 = trackPtr->getFitStatus(rep)->getChi2();
        int trkNdf = trackPtr->getFitStatus(rep)->getNdf();
        int rtrkCharge = trackPtr->getFitStatus(rep)->getCharge();


        printf("===>> Track=%4d  PDG=%5d MC=%9.4f  REC=%12.4f   diff=%9.4f Charge=%d  Chi2=%f  Ndf=%f \n"
               ,iTrack
               ,212
               ,trkmom
               ,mrec
               ,mrec - trkmom
               ,rtrkCharge
               ,trackPtr->getFitStatus(rep)->getChi2()/trackPtr->getFitStatus(rep)->getNdf()
               ,trackPtr->getFitStatus(rep)->getNdf());
    }
            

    
}

//-----------------
// AddHitXYZ
//-----------------
void eicGenFit::AddHitXYZ(float x, float y, float z)
{
	/// Add a hit to the list of hits useing Cartesian coordinates
	trkHit_t *hit = new trkHit_t;
	hit->x = x / 10;
	hit->y = y / 10;
	hit->z = z / 10;
	//hit->chisq = 0.0;
	hits.push_back(hit);
        //printf("x:%f, y:%f, z:%f\n",hit->x,hit->y,hit->z);
	//return NOERROR;
}



//---------------------
void eicGenFit::Clear()
{
  hits.clear();
}

//-----------------
// eicGenFit (Destructor)
//-----------------
eicGenFit::~eicGenFit()
{

  printf("eicGenFit: Hallo World~\n");

}
