#include <JANA/JObject.h>
#include <JANA/JFactory.h>
#include <JANA/JFactoryT.h>
#include <JANA/JEvent.h>
#include "VertexTrackerHit.h"
#include <MinimalistModel/McTrack.h>
#include <MinimalistModel/McFluxHit.h>

// This method gets called to actually produce the MyCluster objects for the event.
// We use "partial template specialization" to define this here in place of the default
// one defined in the templated JFactoryT class.
template<>
void JFactoryT<VertexTrackerHit>::Process(const std::shared_ptr<const JEvent>& event)
{
    using namespace minimodel;
    // Get the MyHit objects for the event. We can grab as many different types of
    // objects we need to here in order to make the MyCluster objects. For this
    // example, only the MyHit objects are needed. Note how we do not need to specify
    // whether these came from a JEventSource or another JFactory (i.e. algorithm).
    // The type of the hits variable is a vector<const MyHit*> here. Using the
    // "auto" type means we only need to type "MyHit" once, minimizing potential
    // type conflicts from copy/paste errors.
    auto mc_hits = event->Get<McFluxHit>();

    std::vector<VertexTrackerHit *> hits;

    // Loop over all MC hits
    for(auto mc_hit : mc_hits){

        // Create an object of the type this factory makes. In this
        // case there is not really any algorithm. The values are
        // just copied from the MC hit.
        auto hit = new VertexTrackerHit;
        hit->planeid = mc_hit->plane_id;
        hit->track_id = mc_hit->track_id;
        hit->Xpos    = mc_hit->x;
        hit->Ypos    = mc_hit->y;
        hit->Zpos    = mc_hit->z;

        // One can add any number of objects as associated objects.
        // This allows you to trace back to the objects that were used
        // to create the object you're working with.
        hit->AddAssociatedObject(mc_hit);

        // Add hits to resulting array
        hits.push_back(hit);
    }
    event->Insert(hits);
}

