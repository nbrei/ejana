// $Id$
//
//    File: VertexTrackerTrack.h
// Created: Thu Jan  5 21:58:34 EST 2017
// Creator: davidl (on Darwin harriet 15.6.0 i386)
//

#ifndef _VertexTrackerTrack_
#define _VertexTrackerTrack_

#include <JANA/JObject.h>

class VertexTrackerTrack:public JObject{
	public:
		JOBJECT_PUBLIC(VertexTrackerTrack);
		double p;
		double theta;
		double phi;
		int    q;
		double chisq;
		double ndf;
};

#endif // _VertexTrackerTrack_

