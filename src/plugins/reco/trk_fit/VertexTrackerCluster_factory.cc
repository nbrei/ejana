#include <iostream>
#include <iomanip>

#include <JANA/JFactory.h>
#include <JANA/JFactoryT.h>
#include <JANA/JEvent.h>
#include "VertexTrackerCluster.h"

// This method gets called to actually produce the VertexTrackerCluster objects for the event.
// We use "partial template specialization" to define this here in place of the default
// one defined in the templated JFactoryT class.
template<>
void JFactoryT<VertexTrackerCluster>::Process(const std::shared_ptr<const JEvent>& event)
{
    using namespace std;

	// Here we want to find hits that belong to the same track.
	// I'm going to cheat here by just peeking at the trackid 
	// of the mchit from which each hit was made.
	auto hits = event->Get<VertexTrackerHit>();
	
	// Container to hold all hits, indexed by trackid
	map<int, vector<const VertexTrackerHit*> > clusters_map;

	// Loop over all hits
	for(const auto& hit : hits){

		// Add this hit to the appropriate cluster
		clusters_map[hit->track_id].push_back(hit);
	}

    std::vector<VertexTrackerCluster* > clusters;

	// Loop over all clusters
	for(const auto &c : clusters_map){
	
		// Create new cluster object and copy hits into it
		auto cluster = new VertexTrackerCluster;
		cluster->hits = c.second;

		// Hand cluster object to JANA
        clusters.push_back(cluster);
	}
	event->Insert(clusters);
}

