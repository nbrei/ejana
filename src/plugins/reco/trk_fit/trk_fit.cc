#include <JANA/JFactoryGenerator.h>
#include <JANA/JApplication.h>
#include <JANA/JFactoryT.h>

#include "VertexTrackerHit.h"
#include "VertexTrackerTrack.h"
#include "VertexTrackerCluster.h"


class JFactoryGenerator_vtx_fit:public JFactoryGenerator{
public:

    void GenerateFactories(JFactorySet *factory_set) override {

        factory_set->Add(new JFactoryT<VertexTrackerHit>());
        factory_set->Add(new JFactoryT<VertexTrackerTrack>());
        factory_set->Add(new JFactoryT<VertexTrackerCluster>());
    }
};


// Routine used to create our JEventProcessor
extern "C"{
    void InitPlugin(JApplication *app){
        InitJANAPlugin(app);
        app->Add(new JFactoryGenerator_vtx_fit());
    }
} // "C"
