// $Id$
//
//    File: VertexTrackerHit.h
// Created: Thu Jan  5 21:59:40 EST 2017
// Creator: davidl (on Darwin harriet 15.6.0 i386)
//

#ifndef VERTEX_TRACKING_HIT_HEADER
#define VERTEX_TRACKING_HIT_HEADER

#include <JANA/JObject.h>

class VertexTrackerHit:public JObject{
	public:
		int planeid;
		double Xpos;
		double Ypos;
		double Zpos;
        uint64_t track_id;
};

#endif // VERTEX_TRACKING_HIT_HEADER

