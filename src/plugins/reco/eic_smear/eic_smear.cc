// Routine used to create our JEventProcessor
#include <JANA/JApplication.h>
#include <JANA/JEventSourceGeneratorT.h>
#include <JANA/JFactoryGenerator.h>
#include <MinimalistModel/McGeneratedParticle.h>
#include "SmearedEvent.h"
#include "SmearingFactory.h"

/// This class is temprorary here while JANA2 API is getting to perfection.
/// Here we just list JFactories defined in this plugin
struct SmearingFactoryGenerator:public JFactoryGenerator {

    explicit SmearingFactoryGenerator(JApplication *app) {
        _app = app;
    }

    void GenerateFactories( JFactorySet *factory_set) override {
        factory_set->Add(new SmearingFactory(_app->GetJParameterManager()));
    }

private:
    JApplication *_app;
};



extern "C"{
    void InitPlugin(JApplication *app)
    {
        InitJANAPlugin(app);

        app->Add(new SmearingFactoryGenerator(app));

        //app->Add(new EicSmearProcessor(app));
    }
}