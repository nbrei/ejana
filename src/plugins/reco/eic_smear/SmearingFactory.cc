#include "SmearingFactory.h"

// TODO organize includes
#include <JANA/JEventProcessor.h>
#include <JANA/JEvent.h>
#include "eicsmear/erhic/VirtualParticle.h"
#include "eicsmear/smear/Acceptance.h"
#include "eicsmear/smear/Device.h"
#include "eicsmear/smear/Detector.h"
#include "eicsmear/smear/Smearer.h"
#include "eicsmear/smear/ParticleMCS.h"
#include "eicsmear/smear/PerfectID.h"
#include <eicsmear/smear/Smear.h>
#include <eicsmear/erhic/ParticleMC.h>
#include "Math/Vector4D.h"
#include <ejana/EServicePool.h>
#include <MinimalistModel/McGeneratedParticle.h>

#include <TClingRuntime.h>

#include <fmt/format.h>     // For format and print functions
#include <cstdlib>
#include <cmath>
#include <mutex>

#include "ZeusDetector.h"
#include "BeASTDetector.h"
#include "ePHENIXDetector.h"
#include "JleicSmear.h"

void SmearingFactory::Init() {

    _beast_detector = BuildBeAST();
    _zeus_detector = BuildZeus();
    _ephoenix_detector = BuildEphoenix();

    //jleic_eic_smear_detector = BuildJLEIC();
    // Verbosity level. 0 = show nothing. 1 - show some. 2 - mad printer
    // SetDefaultParameter actually sets the parameter value from arguments if it is specified
    _pm->SetDefaultParameter("eic_smear:verbose", _verbose, "Plugin output level. 0-almost nothing, 1-some, 2-everything");
    _pm->SetDefaultParameter("eic_smear:detector", _detector_name, "Detector name: beast, jleic, zeus");
}

void SmearingFactory::Process(const std::shared_ptr<const JEvent> &event) {
    using namespace fmt;
    using namespace std;

    // Get the inputs needed for this factory.
    auto source_particles = event->Get<minimodel::McGeneratedParticle>();

    vector<minimodel::McGeneratedParticle*> dest_particles;

    for (auto source_particle : source_particles) {
        auto dest_particle = new minimodel::McGeneratedParticle();
        *dest_particle = *source_particle;  // Copy

        _stat.total_particles++;


        if (_detector_name == "jleic") {
            SmearPrticleJleic(dest_particle, _verbose);

            if (_verbose>=2) {
                //
                fmt::print("pdg {:<7}   P in {:<10} {:<10}  E in {:<10}  {:<10}\n",
                           source_particle->pdg,
                           source_particle->p, dest_particle->p,
                           source_particle->tot_e, dest_particle->tot_e);
            }

            // jleic smearing alwais smears everything
            _stat.smear_e_smear_p++;

        }
        else if (_detector_name == "beast" ||
                 _detector_name == "zeus"  ||
                 _detector_name == "ephoenix") {
            // EIC smear
            erhic::ParticleMC not_smeared_prt;
            not_smeared_prt.SetId(source_particle->pdg);           // PDG particle code
            not_smeared_prt.SetIndex(source_particle->id);         // Particle index in event
            not_smeared_prt.SetStatus(source_particle->gen_code);  // Particle status code: like in PYTHIA, Beagle, etc
            not_smeared_prt.Set4Vector(TLorentzVector(source_particle->px, source_particle->py, source_particle->pz, source_particle->tot_e));
            not_smeared_prt.SetP(source_particle->p);

            Smear::ParticleMCS * smeared_prt;
            if(_detector_name == "beast") {
                smeared_prt = _beast_detector.Smear(not_smeared_prt);
            }
            else if (_detector_name == "ephoenix") {
                smeared_prt = _ephoenix_detector.Smear(not_smeared_prt);
            }
            else {
                smeared_prt = _zeus_detector.Smear(not_smeared_prt);
            }

            if(!smeared_prt) {
                _stat.null_particles++;
                continue;
            }

            // fmt::print("P {:<10} {:<10}  E {:<10}  {:<10}\n", source_particle->p, smeared_prt->GetP(), source_particle->tot_e, smeared_prt->GetE());

            // Check odds of eic-smear smearing
            double in_p = source_particle->p;
            double sm_p = smeared_prt->GetP();
            double in_e = source_particle->tot_e;
            double sm_e = smeared_prt->GetE();

            // Eic smear return non zero p
            bool zero_p = TMath::Abs(in_p)>0.001 && TMath::Abs(sm_p)<0.00001;
            bool zero_e = TMath::Abs(in_e)>0.001 && TMath::Abs(sm_e)<0.00001;

            if(zero_p && zero_e) {
                _stat.zero_e_zero_p ++;
                continue; // we don't take such particle
            }
            else if (zero_p) {
                _stat.smear_e_zero_p++;
                continue; // we don't take such particle
            }
            else if (zero_e) {
                _stat.zero_e_smear_p++;
                continue; // we don't take such particle
            }
            else {
                _stat.smear_e_smear_p++;
                // that! is the particle we take...
            }

            // Copy back the particle
            dest_particle->px = smeared_prt->GetPx();
            dest_particle->py = smeared_prt->GetPy();
            dest_particle->pz = smeared_prt->GetPz();
            dest_particle->tot_e = smeared_prt->GetE();
        }
        else {
            std::call_once(WarnNoNameWarningOnceFlag(), [this](){
                print("eic_smear: (!) WARNING detector name = '{}' is not set or not known. \n"
                      "Set 'eic_smear:detector' parameter to 'jleic', 'beast' or 'zeus'. This={}\n", this->_detector_name, (size_t)(void*)this);
            });
        }

        dest_particles.push_back(dest_particle);
    }

    Set(std::move(dest_particles));
}

void SmearingFactory::PrintSmearStats() {
    fmt::print("SmearingFactory statistics:\n");
    fmt::print("   total_particles = {:<10} \n", _stat.total_particles);
    fmt::print("   null_particles  = {:<10} (not smeared)\n", _stat.null_particles);
    fmt::print("   zero_e_zero_p   = {:<10} (not smeared)\n", _stat.zero_e_zero_p);
    fmt::print("   zero_e_smear_p  = {:<10} (partly smeared)\n", _stat.zero_e_smear_p);
    fmt::print("   smear_e_zero_p  = {:<10} (partly smeared)\n", _stat.smear_e_zero_p);
    fmt::print("   smear_e_smear_p = {:<10} (smeared)\n", _stat.smear_e_smear_p);


}

