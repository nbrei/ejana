#ifndef EJANA_BEASTDETECTOR_H
#define EJANA_BEASTDETECTOR_H

#include "eicsmear/smear/Detector.h"

/// Builds definition of BeAST detector
Smear::Detector BuildBeAST();

#endif //EJANA_BEASTDETECTOR_H
