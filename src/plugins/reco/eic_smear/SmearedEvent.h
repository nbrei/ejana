//
// Created by romanov on 7/5/2019.
//

#ifndef EJANA_SMEAREDEVENT_H
#define EJANA_SMEAREDEVENT_H

#include <JANA/JEventProcessor.h>
#include <JANA/JEvent.h>
#include "eicsmear/erhic/VirtualParticle.h"
#include "eicsmear/smear/Acceptance.h"
#include "eicsmear/smear/Device.h"
#include "eicsmear/smear/Detector.h"
#include "eicsmear/smear/Smearer.h"
#include "eicsmear/smear/ParticleMCS.h"
#include "eicsmear/smear/PerfectID.h"
#include <eicsmear/smear/Smear.h>
#include <eicsmear/erhic/ParticleMC.h>
#include "Math/Vector4D.h"
#include <ejana/EServicePool.h>
#include <MinimalistModel/McGeneratedParticle.h>
#include <TClingRuntime.h>
#include <fmt/format.h>     // For format and print functions

/**
 Convert pseudorapidity (eta) to polar angle (theta) in radians.
 Make use of TLorentzVector to do eta-to-theta conversion.
 */
double etaToTheta(const double eta) {
    TLorentzVector v;
    v.SetPtEtaPhiM(1., eta, 0., 0.);
    return v.Theta();
}

/**
 Convert and angle in degrees to one in radians.
 */
double degreesToRadians(double degrees) {
    return degrees / 180. * TMath::Pi();
}

/**
 Smearing parameterisations for the ZEUS detector.

 See JHEP05 (2009) 108.

 Note: you must gSystem->Load("libeicsmear") BEFORE loading this script.
 */
Smear::Detector BuildDetector() {
    // The central calorimeter, providing both electromagnetic and hadronic
    // calorimetry, but with different resolutions.
    // Note that this excludes the forward calorimeter.
    Smear::Device emCal(Smear::kE, "0.18*sqrt(E)", Smear::kElectromagnetic);
    Smear::Device hCal(Smear::kE, "0.35*sqrt(E)", Smear::kHadronic);

    // Calorimeter acceptance is +/- 4 in pseudorapidity.
    // Note that 4 is before -4 as eta-max corresponds to theta-min.
    Smear::Acceptance::Zone cal(etaToTheta(4.), etaToTheta(-4.));
    emCal.Accept.AddZone(cal);
    hCal.Accept.AddZone(cal);

    // ZEUS tracking acceptance covers 15 to 164 degrees in polar angle.
    // This is approximately +/- 2 in pseudorapidity.
    // Define with two volumes, for momentum and theta.
    Smear::Device theta(Smear::kTheta, "0.0005*P + 0.003");
    Smear::Device momentum(Smear::kP, "0.0085*P + 0.0025*P*P");
    Smear::Acceptance::Zone tracking(degreesToRadians(15.), degreesToRadians(164.));
    theta.Accept.AddZone(tracking);
    momentum.Accept.AddZone(tracking);

    // The acceptance of the Roman pots is a complicated function of
    // angle and momentum, and not easy to define using the available
    // Acceptance implementation. For completeness the parameterisations
    // are included below in case they are of use, and users wish to
    // define their own acceptance some way.
    // Smear::Device romanTran(Smear::kPt, "0.005");
    // Smear::Device romanLong(Smear::kPz, "0.005 * pZ");
    // There is no parameterisation for phi, so add a dummy device:
    Smear::Device phi(Smear::kPhi);

    // Add all the devices to the detector.
    Smear::Detector zeus;
    zeus.AddDevice(emCal);
    zeus.AddDevice(hCal);
    zeus.AddDevice(theta);
    zeus.AddDevice(momentum);
    zeus.AddDevice(phi);

    // zeus.AddDevice(romanTran);
    // zeus.AddDevice(romanLong);
    zeus.SetEventKinematicsCalculator("NM JB DA");
    return zeus;
}



class EicSmearProcessor : public JEventProcessor {
public:
    // Constructor just applies
    explicit EicSmearProcessor(JApplication *app = nullptr) :
            JEventProcessor(app),
            services(app) {
        /*
         * #include "TInterpreter.h"

extern "C" int printf(const char*,...);
struct WithDtor {
   WithDtor(): fBuf("IAmWithDtor!") {
      ++fgInstanceCount;
      printf("WithDtor(): %d\n", fgInstanceCount);
   }
   WithDtor(const WithDtor&) {
      ++fgInstanceCount;
      printf("WithDtor(const WithDtor&) %d\n",
             fgInstanceCount);
   }
   ~WithDtor() {
      --fgInstanceCount;
      printf("~WithDtor() %d\n", fgInstanceCount);
   }
   const char* ident() { return fBuf; }
   const char* fBuf;
   static int fgInstanceCount;
};

int WithDtor::fgInstanceCount = 0;

WithDtor returnsWithDtor() {
   WithDtor obj;
   printf("About to return a WithDtor\n");
   return obj;
}

void runInterpreterValue() {
   ClassInfo_t* ci = gInterpreter->ClassInfo_Factory("");
   CallFunc_t* cf = gInterpreter->CallFunc_Factory();
   gInterpreter->CallFunc_SetFunc(cf, ci, "returnsWithDtor", "", 0);

   TInterpreterValue* val = gInterpreter->CreateTemporary();
   gInterpreter->CallFunc_Exec(cf, 0, *val);

   WithDtor* withDtor = (WithDtor*)val->GetAsPointer();
   printf("Ident: %s\nNow deleting TInterpreterValue\n",
          withDtor->ident());
   delete val;
   printf("Now all WithDor should be gone; we have %d left\n",
          WithDtor::fgInstanceCount);
}
         */


//        vector<JFactoryGenerator*> gens;
//        app->GetJFactoryGenerators(gens);
//        for(auto gen: gens) {
//            gen->
//
//        }
//        app->
//        auto factory_set = app->GetFactorySet();
//        std::map<std::pair<std::type_index, std::string>, JFactory*> factories = factory_set->;
//        for(auto &pair: factories) {
//            fmt::print("{}\n", pair.first.first.name(), pair.first.second, pair.second->GetName());
//        }
    };

    // This is called once before the first call to the Process method
    // below. You may, for example, want to open an output file here.
    // Only one thread will call this.
    void Init() override {
        detector = BuildDetector();
    };

    //----------------------------
    // Process
    //
    // This is called for every event. Multiple threads may call this
    // simultaneously. If you write something to an output file here
    // then make sure to protect it with a mutex or similar mechanism.
    // Minimize what is done while locked since that directly affects
    // the multi-threaded performance.
    void Process(const std::shared_ptr<const JEvent> &event) override {
        //detector.FillEventKinematics()
        using namespace minimodel;

        // Get the inputs needed for this factory.
        auto particles = event->Get<McGeneratedParticle>();
        for(auto& mm_prt: particles) {
            erhic::ParticleMC not_smeared_prt;
            not_smeared_prt.SetId(mm_prt->pdg);       // PDG particle code
            not_smeared_prt.SetIndex(mm_prt->id);    // Particle index in event
            not_smeared_prt.SetStatus(mm_prt->gen_code);  // Particle status code: like in PYTHIA, Beagle, etc
            not_smeared_prt.Set4Vector(TLorentzVector(mm_prt->px, mm_prt->py, mm_prt->pz, mm_prt->tot_e));
            not_smeared_prt.SetP(mm_prt->p);
            auto smeared_prt = detector.Smear(not_smeared_prt);
            fmt::print("P {:<10} {:<10}  E {:<10}  {:<10}", mm_prt->p, smeared_prt->GetP(), mm_prt->tot_e, smeared_prt->GetE());
        }
    };


    //----------------------------
    // Finish
    //
    // This is called once after all events have been processed. You may,
    // for example, want to close an output file here.
    // Only one thread will call this.
    void Finish() override {

    }

private:
    ej::EServicePool services;
    int verbose = 0;                // verbose output level 0 = nothing, 1 info, 2 debug
    Smear::Detector detector;

};

#endif //EJANA_SMEAREDEVENT_H
