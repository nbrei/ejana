#include <JANA/JApplication.h>
#include <JANA/JEventSourceGeneratorT.h>
#include <JANA/JFactoryGenerator.h>


#include <MinimalistModel/McGeneratedParticle.h>

#include "JEventSource_beagle.h"
#include "BeagleParticle.h"


extern "C"{
    void InitPlugin(JApplication *app)
    {
        InitJANAPlugin(app);

        app->Add(new JEventSourceGeneratorT<JEventSource_beagle>(app));
        app->Add(new JFactoryGeneratorT<JFactoryT<ej::BeagleParticle>>());
        app->Add(new JFactoryGeneratorT<JFactoryT<minimodel::McGeneratedParticle>>());
    }
}