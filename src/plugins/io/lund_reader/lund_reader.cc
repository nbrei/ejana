#include "JEventSource_lund.h"
#include "LundEventData.h"

// Routine used to create our JEventProcessor
#include <JANA/JApplication.h>
#include <JANA/JEventSourceGeneratorT.h>
#include <JANA/JFactoryGenerator.h>
#include <MinimalistModel/McGeneratedParticle.h>

/// This class is temprorary here while JANA2 API is getting to perfection.
/// Here we just list JFactories defined in this plugin
struct JFactoryGenerator_lund_reader:public JFactoryGenerator {
    void GenerateFactories(JFactorySet *factory_set) override {

        factory_set->Add( new JFactoryT<ej::LundEventData>());
        //factory_set->Add( new JFactoryT<ej::LundParticle>());
        factory_set->Add( new JFactoryT<minimodel::McGeneratedParticle>());
    }
};

extern "C"{
    void InitPlugin(JApplication *app)
    {
        InitJANAPlugin(app);

        app->Add(new JEventSourceGeneratorT<JEventSource_lund>(app));
        app->Add(new JFactoryGenerator_lund_reader());
    }
}