//
// Created by Alexander on 16.07.2019.
//

//Todo: the meaning of all the 'varN' fields is unknown to me

#ifndef EJANA_LUNDEVENTDATA_H
#define EJANA_LUNDEVENTDATA_H

#include <vector>
#include <utility>

#include <JANA/JObject.h>
#include <ejana/TextEventFileReader.h>

#include "LundParticle.h"

class JApplication;

namespace ej {
    class LundEventData : public JObject {
    public:
        LundEventData() {}

        void Parse() {

            this->started_at_line = text_event->started_at_line;
            auto event_tokens = text_event->event_values[0];

            this->nrTracks = std::stoi(event_tokens[0]); // I ievent - event num
            this->var1 = std::stod(event_tokens[1]);
            this->var2 = std::stod(event_tokens[2]);
            this->var3 = std::stod(event_tokens[3]);
            this->var4 = std::stod(event_tokens[4]);
            this->var5 = std::stod(event_tokens[5]);
            this->var6 = std::stod(event_tokens[6]);
            this->var7 = std::stod(event_tokens[7]);
            this->var8 = std::stod(event_tokens[8]);
            this->var9 = std::stod(event_tokens[9]);

            this->is_parsed = true;
        }


        ~LundEventData() override = default;

        uint64_t started_at_line;

        int nrTracks;        //1   I  number of particles
        double var1;         //2   D  some variable
        double var2;         //3   D  some variable
        double var3;         //4   D  some variable
        double var4;         //5   D  some variable
        double var5;         //6   D  some variable
        double var6;         //7   D  some variable
        double var7;         //8   D  some variable
        double var8;         //9   D  some variable
        double var9;         //10  D  some variable

        /// Collection of primary vertexes (that came from a generator)
        std::vector<LundParticle *> particles; // Primary particles (that comes from a generator)

        std::unique_ptr<ej::TextFileEvent> text_event;
        bool is_parsed = false;
    private:
    };


}


#endif //EJANA_LUNDEVENTDATA_H
