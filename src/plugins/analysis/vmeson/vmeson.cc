#include <JANA/JEventProcessor.h>
#include <JANA/JFactoryGenerator.h>


#include "VectorMesonProcessor.h"

/// This class is temprorary here while JANA2 API is getting to perfection.
/// Here we just list JFactories defined in this plugin
struct VectorMesonInputParticleGenerator:public JFactoryGenerator {
    void GenerateFactories(JFactorySet *factory_set) override {
        factory_set->Add( new JFactoryT<VMesonInputParticle>());
    }
};

extern "C"
{
    void InitPlugin(JApplication *app)
    {
        InitJANAPlugin(app);

        app->Add(new VectorMesonProcessor(app));
        app->Add(new VectorMesonInputParticleGenerator());
    }
}
