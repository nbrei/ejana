#include "TRandom.h"
#include "TLorentzVector.h"
#include "TVector3.h"

#include <fmt/core.h>

#include <JANA/JEventProcessor.h>
#include <JANA/JEvent.h>

#include <MinimalistModel/McFluxHit.h>
#include <MinimalistModel/McGeneratedParticle.h>
#include <io/beagle_reader/BeagleEventData.h>

#include "VectorMesonProcessor.h"
#include "Kin_Variables.h"


// TODO repalce by some 'conventional' PDG info provider. Like TDatabasePDG
//==================================================================
double MassPI = 0.139570,
        MassK = 0.493677,
        MassP = 0.93827,
        MassE = 0.000511,
        MassN = 0.939565,
        MassMU = 0.105658;
//==================================================================


using namespace std;
using namespace fmt;

struct VMesonRecord
{
    double Q2_true;
    double Q2_smeared;
    double Q2_trkng;
    double Xel,
            Yel,
            Q2el;
    double Xgen,
            Ygen,
            Q2gen;
    double Eth;
    double Ee;
    double p;
    double pt;
};

static VMesonRecord vmrec;

void VectorMesonProcessor::Init()
{
    ///  Called once at program start.
    jout << "VectorMesonProcessor: Init()" << endl;

    // initialize class that holds histograms and other root objects
    // 'services' is a service locator, we ask it for TFile
     root_out.init(services.Get<TFile>());
//    root_out.tree_rec_e->Branch("Q2_true", &vmrec.Q2_true);
//    root_out.tree_rec_e->Branch("Q2_smeared", &vmrec.Q2_smeared);
//    root_out.tree_rec_e->Branch("Q2_trkng", &vmrec.Q2_trkng);
//    root_out.tree_rec_e->Branch("Xel", &vmrec.Xel);
//    root_out.tree_rec_e->Branch("Yel", &vmrec.Yel);
//    root_out.tree_rec_e->Branch("Q2el", &vmrec.Q2el);
//    root_out.tree_rec_e->Branch("Xgen", &vmrec.Xgen);
//    root_out.tree_rec_e->Branch("Ygen", &vmrec.Ygen);
//    root_out.tree_rec_e->Branch("Q2gen", &vmrec.Q2gen);
//    root_out.tree_rec_e->Branch("Eth", &vmrec.Eth);
//    root_out.tree_rec_e->Branch("Ee", &vmrec.Ee);
//    root_out.tree_rec_e->Branch("p", &vmrec.p);
//    root_out.tree_rec_e->Branch("pt", &vmrec.pt);
    //

    // Ask service locator for parameter manager. We want to get this plugin parameters.
    auto pm = services.Get<JParameterManager>();

    // Verbosity level. 0 = show nothing. 1 - show some. 2 - mad printer
    // SetDefaultParameter actually sets the parameter value from arguments if it is specified
    verbose = 0;
    pm->SetDefaultParameter("vmeson:verbose", verbose, "Plugin output level. 0-almost nothing, 1-some, 2-everything");

    // Beams energies
    pm->SetDefaultParameter("vmeson:e_beam_energy", e_beam_energy, "Energy of colliding electron beam");
    pm->SetDefaultParameter("vmeson:ion_beam_energy", ion_beam_energy, "Energy of colliding ion beam");
    if (verbose) {
        print("Parameters:\n");
        print("  vmeson:verbose:         {}\n", verbose);
        print("  vmeson:e_beam_energy:   {}\n", e_beam_energy);
        print("  vmeson:ion_beam_energy: {}\n", ion_beam_energy);
    }
}


void VectorMesonProcessor::Process(const std::shared_ptr<const JEvent> &event)
{
    ///< Called every event.

    using namespace fmt;
    using namespace std;
    using namespace minimodel;
    TLorentzVector pbeam, nbeam, ebeam;
    pbeam.SetXYZM(ion_beam_energy * sin(0.05), 0., ion_beam_energy * cos(0.05), MassP);
    nbeam.SetXYZM(ion_beam_energy * sin(0.05), 0., ion_beam_energy * cos(0.05), MassN);
    ebeam.SetXYZM(e_beam_energy, 0., -e_beam_energy, MassE);

    // >oO debug printing
    if (verbose >= 2) {
        print("=============================================================\n");
        print("========================New EVENT {} ===========================\n ", event->GetEventNumber());
        print("=============================================================\n");
    }


    auto beable_event = event->GetSingle<ej::BeagleEventData>();

    if (verbose >= 2) {
        print("Beagle event data: {} {} {} \n", beable_event->truex, beable_event->trueQ2, beable_event->t_hat);
    }

    // Get the inputs needed for this factory.
    auto gen_particles = event->Get<VMesonInputParticle>();
    TLorentzVector proton, neutron, electron;
    TLorentzVector a, b, momentum_t, initial_d, scattered_d;

    TVector3 a3, b3, c3, d3;
    double Xel, Yel, Q2el;
    double Xtrue, Q2true, Ytrue, W2true, Nutrue;

    double my_theta, my_pt = 0, my_t_n, my_t_p, my_t_d, t_true;
    t_true = -beable_event->t_hat;

    //*************************Loop over Generated particles *********************************
    for (auto particle0: gen_particles) {   // --- electrons
        if (particle0->pdg != 11) continue;

        electron.SetXYZM(particle0->p.px(), particle0->p.py(), particle0->p.pz(), MassE);

        double Eth = particle0->p.Theta();
        double Ee = electron.E();
        double Ee_smear = Ee;  // should work if EIC smear is there.
        // gRandom->Gaus(Ee, 0.1*sqrt(Ee) + 0.01*Ee + 0.005);

        double ion_beam_E = 2. * ion_beam_energy * cos(0.05);
        GEN_El_XYQ2(Ee_smear, Eth, e_beam_energy, ion_beam_E, Xel, Yel, Q2el);

        Xtrue = beable_event->truex;
        Q2true = beable_event->trueQ2;
        Ytrue = beable_event->truey;
        W2true = beable_event->trueW2;
        Nutrue = beable_event->trueNu;

//        // Fill TTree record:
//        {
//            std::lock_guard<std::recursive_mutex> locker2(root_out.lock);
//            vmrec.Q2_true = log10(Q2gen);
//            vmrec.Q2_smeared = log10(Q2el);
//            vmrec.Q2_trkng = 0;
//            vmrec.Xel = Xel;
//            vmrec.Yel = Yel;
//            vmrec.Q2el = Q2el;
//            vmrec.Xgen = Xgen;
//            vmrec.Ygen = Ygen;
//            vmrec.Q2gen = Q2gen;
//            vmrec.Eth = Eth;
//            vmrec.Ee = Ee;
//            vmrec.p = ptot;
//            vmrec.pt = pt;
//            root_out.tree_rec_e->Fill();
//        }

        if (verbose >= 2) {
            print(">>>>>>>>>>>>>>>>> electron found <<<<<<<<<<<<<<<<<<<<<<<<<<<\n");
            print(" Eebeam {}  Epbeam {} \n", ebeam.E(), pbeam.E());
            print(" xtrue {} xel {}  Q2true {} Q2el {} ytrue {} yel {} W2_true {} Nu_True {}  t=={} \n ",
                  Xtrue, Xel, Q2true, Q2el, Ytrue, Yel, W2true, Nutrue, beable_event->t_hat);
        }

        root_out.h2_XQ2_true_log->Fill(log10(Xtrue), log10(Q2true));
        root_out.h2_XQ2_true->Fill(Xtrue, Q2true);
        root_out.hQ2_true->Fill(Q2true);
        root_out.hX_true->Fill(Xtrue);
        root_out.hY_true->Fill(Ytrue);
        root_out.hW2_true->Fill(W2true);
        root_out.hNu_true->Fill(Nutrue);
        root_out.e_theta->Fill(particle0->p.Theta() * 180 / 3.1415);
        root_out.e_eta->Fill(particle0->p.Eta());
        for (auto particle1: gen_particles) {  // ---- protons and neutrons -----
            std::lock_guard<std::recursive_mutex> locker(root_out.lock);

            if (particle1->pdg != 2112) { continue; }; // --- all neutrons ---


            // print(" generated particles PID {} px={} py={} pz={}\\\n", particle->pid, particle->px,particle->py,particle->pz);
            double px_n = particle1->p.px();
            double py_n = particle1->p.py();
            double pz_n = particle1->p.pz();
            neutron.SetXYZM(px_n, py_n, pz_n, MassN);
            //   --- calculate kinematic variables using electron only method -------------

            if (verbose >= 2) {
                printf("................. neutron found ...............\n");
                print("Neutron beam pt={} theta = {}\n", nbeam.Pt(), nbeam.Theta() * 1000.);
                print("Neutron scatetred pt={} theta = {}\n", neutron.Pt(), neutron.Theta() * 1000.);
            }

            for (auto particle3: gen_particles) {  // ---- protons and neutrons -----
                if (particle3->pdg != 2212) { continue; }; // --- all protons---


                // print(" generated particles PID {} px={} py={} pz={}\\\n", particle->pid, particle->px,particle->py,particle->pz);
                double px_p = particle3->p.px();
                double py_p = particle3->p.py();
                double pz_p = particle3->p.pz();
                proton.SetXYZM(px_p, py_p, pz_p, MassP);
                //   --- calculate kinematic variables using electron only method -------------
                if (verbose >= 2) {
                    printf("................. proton found ...............\n");
                    print("Proton beam :  pt ={} theta={} mrad\n", pbeam.Pt(), pbeam.Theta() * 1000.);
                    print("Proton scatetred :  pt ={} theta={} mrad \n", proton.Pt(), proton.Theta() * 1000.);
                }

                b3.SetXYZ(px_p, py_p, pz_p);
                //     b3.RotateX(0.05);
                a3.SetXYZ(px_n, py_n, pz_n);
                //   a3.RotateZ(0.05);

                initial_d = pbeam + nbeam;   // initial D-beam
                initial_d.RotateY(-0.05);
                scattered_d = proton + neutron; // Final exclusive D

                momentum_t = scattered_d - initial_d;


                c3 = b3 + a3; // summ of final staters p and n


                //   double x_L = neutron.E() / nbeam.E();
                double x_L = scattered_d.Pz() / initial_d.Pz();

                my_t_n = a3.Pt() * a3.Pt() / x_L;
                my_t_d = -momentum_t.Mag2();
                my_t_p = c3.Pt() * c3.Pt() / x_L;

                if (verbose >= 2) {
                    print(" momentum_t : theta {}(mrad) pt {} t {} \n", momentum_t.Theta() * 1000., momentum_t.Pt(), momentum_t.Pt() * momentum_t.Pt());
                    print("D (d) ::::: initital beam pt = {}, theta = {} (mrad) \n", initial_d.Pt(), initial_d.Theta() * 1000.);
                    print("D' (c) ::::: final d  pt = {}, theta = {} (mrad)t= {}\n", scattered_d.Pt(), scattered_d.Theta() * 1000., scattered_d.Pt() * scattered_d.Pt());
                    print("neutron:::::  pt = {}, theta = {} (mrad) \n ", a3.Pt(), a3.Theta() * 1000.);
                    print("proton:::::  pt = {}, theta = {}(mrad)\n ", b3.Pt(), b3.Theta() * 1000.);
                    //    if(abs(a3.Pt()-b3.Pt())>1) continue; // requirements of D' as a system.
                    print("  t :::::  true_t={} t_d= {} t_n ={} t_p={},\n ", t_true, my_t_d, my_t_n, my_t_p);
                }

                //               my_theta = particle1->p.Theta();

                for (auto particle2: gen_particles) {

                    if (particle2->pdg != 443) { continue; };
                    if (x_L < 0.85) { continue; };


                    double t_jpsi = (particle0->p.px() + particle2->p.px()) * (particle0->p.px() + particle2->p.px()) +
                                    (particle0->p.py() + particle2->p.py()) * (particle0->p.py() + particle2->p.py());
                    double my_W2 = 2 * (ion_beam_energy) * (particle2->p.E() - particle2->p.pz());



                    //    double my_t =        my_t_d;
                    //  my_theta= scattered_d.Theta()*1000.;
                    double my_t;
                    if (a3.Theta() / b3.Theta() < 1) {
                        my_t = my_t_d;
                        my_theta = b3.Theta() * 1000.;
                    } else {
                        my_t = my_t_d;
                        my_theta = a3.Theta() * 1000.;
                    }
                    root_out.h2_mytheta_myt->Fill(my_theta, my_t);

                    root_out.h2_theta_t_jpsi->Fill(my_theta, t_jpsi);

                    //    my_pt = t_jpsi;
                    my_pt = scattered_d.Pt();

                    root_out.h_Theta_jpsi->Fill(cos(particle2->p.Theta()));
                    root_out.h_W2_jpsi->Fill(my_W2);

                    root_out.h_t_jpsi->Fill(t_jpsi);
                    root_out.h_myt->Fill(my_t);
                    root_out.h_myt_d->Fill(my_t_d);
                    root_out.h_myt_n->Fill(my_t_n);

                    root_out.h_theta_n->Fill(a3.Theta() * 1000.);
                    root_out.h_theta_p->Fill(b3.Theta() * 1000.);
                    root_out.h2_theta_pn->Fill(b3.Theta() * 1000., a3.Theta() * 1000.);

                    root_out.h_theta_d->Fill(scattered_d.Theta() * 1000.);
                    root_out.h_mytheta->Fill(my_theta);


                    if (x_L < 1.) root_out.h_xL->Fill(x_L);
                    if (x_L < 1.) root_out.h2_xL->Fill(my_theta, x_L);

                    root_out.h2_xL_ptn->Fill(x_L, my_pt);

                    root_out.h2_xL_t->Fill(x_L, my_t);

                    root_out.h_pt_jpsi->Fill(particle2->p.pt());
                    root_out.h_t_jpsi->Fill(my_t);
                    //         root_out.h2_mytheta_t->Fill(my_theta, particle2->p.pt() * particle2->p.pt());


                    root_out.h2_theta_t_true->Fill(my_theta, t_true);
                    root_out.h2_theta_t_n->Fill(a3.Theta() * 1000., my_t_n);
                    root_out.h2_theta_t_n->Fill(b3.Theta() * 1000., my_t_p);

                    root_out.h2_theta_t_d->Fill(scattered_d.Theta() * 1000., my_t_d);


                    root_out.h2_my_t_ptn->Fill(t_true, my_pt);
                    root_out.h2_my_t_ptn2->Fill(t_true, my_pt * my_pt);
                    root_out.h2_t_true_jpsi->Fill(-beable_event->t_hat, my_t);

                    root_out.h2_mytheta_myptn->Fill(my_theta, my_pt);
                    root_out.h2_mytheta_myptn2->Fill(my_theta, my_pt * my_pt);
                    root_out.h2_mytheta_t_true->Fill(my_theta, t_true);
                    root_out.h2_E_mytheta->Fill(scattered_d.E(), scattered_d.Theta() * 1000.);

                    root_out.h_pt_d->Fill(scattered_d.Pt());
                    root_out.h_pt_n->Fill(neutron.Pt());

                    root_out.h_t_true->Fill(-beable_event->t_hat);

                    if (verbose >= 2) {
                        print(" -----------------J/psi found  -----------------------------\n ");
                        print("  J/PSI::  theta ={} (mrad)  PT= {},  pt^2_jpsi={} t_jpsi_e {} my W2 {}  \n",
                              particle2->p.Theta() * 1000., particle2->p.pt(),
                              particle2->p.pt() * particle2->p.pt(), t_jpsi, my_W2);
                        print("xL={}\n", x_L);
//                        if(root_outputFile && root_outputFile->IsOpen()) {
//				            // Write file
//                            root_outputFile->Write("+");
//			            }
                    }


                    if (abs(my_theta) < 10.) {
                        root_out.h_myptn10->Fill(my_pt);
                        root_out.h_t_true10->Fill(-beable_event->t_hat);


                        root_out.h_pt_jpsi10->Fill(particle2->p.pt());
                        root_out.h_t_jpsi10->Fill(my_t);
                    }
                    if (abs(my_theta) < 5) {


                        root_out.h_myptn5->Fill(my_pt);
                        root_out.h_t_true5->Fill(-beable_event->t_hat);

                        root_out.h_pt_jpsi5->Fill(particle2->p.pt());
                        root_out.h_t_jpsi5->Fill(my_t);
                    }
                    if (abs(my_theta) < 2.5) {
                        root_out.h_myptn2->Fill(my_pt);
                        root_out.h_t_true2->Fill(-beable_event->t_hat);

                        root_out.h_pt_jpsi2->Fill(particle2->p.pt());
                        root_out.h_t_jpsi2->Fill(my_t);
                    }
                }





//        TLorentzVector  mykin_t = proton - neutron;
//
//        double my_t = mykin_t.Mag2();
                //       printf("my_t= %f \n",my_t);
//
//
//            if(particle->pdg == 11) {
//            double Xel,Yel,Q2el;
//            double Xgen,Ygen,Q2gen;
//            double Ebeam=10.;
//            double Pbeam=100.;
//            double ptot = particle->p;
//            double pt = sqrt(particle->px*particle->px+ particle->py*particle->py);
//            double Eth=particle->pz/ptot;
//            double Ee=sqrt(ptot*ptot + MassE*MassE);
//
//            double PISL[4] = {0, 0, Ebeam, 0};
//            PISL[3] = sqrt(Ebeam*Ebeam+MassE*MassE);
//            double PISP[4] = {0, 0, -Pbeam, 0};
//            PISP[3] = sqrt(Pbeam*Pbeam+MassP*MassP);
//            double PFSL[4];
//            PFSL[0] = particle->px;
//            PFSL[1] = particle->py;
//            PFSL[2] = particle->pz;
//            PFSL[3] = Ee;
//
//            int rc = GEN_XYQ2(PFSL,PISL,PISP,Xgen,Ygen,Q2gen);
//
//            root_out.hXQ2_true->Fill(log10(Xgen),log10(Q2gen));
//            root_out.hQ2_true->Fill(Q2gen);
//            root_out.hX_true->Fill(Xgen);
//            root_out.hY_true->Fill(Ygen);
//
//            double Ee_smear = gRandom->Gaus(Ee, 0.1*sqrt(Ee) + 0.01*Ee + 0.005);
//            int rc1 = GEN_El_XYQ2(Ee_smear, Eth, Ebeam, Pbeam, Xel, Yel, Q2el);
//            root_out.hXQ2_smear->Fill(log10(Xel),log10(Q2el));
//            root_out.hpt_eth->Fill(Eth, pt);
//

//
//            // >oO debug printing
//            if(verbose>1) {
//                printf("Xgen=%f, Ygen=%f, Q2gen=%f \n",Xgen,Ygen,Q2gen);
//                printf("Xel=%f, Yel=%f, Q2el=%f \n",Xel,Yel,Q2el);
//            }
                //       }
            }
        }
    }
}


void VectorMesonProcessor::Finish()
{
    ///< Called after last event of last event source has been processed.
    jout << "VectorMesonProcessor::Finish(). Cleanup" << endl;
}


template<>
void JFactoryT<VMesonInputParticle>::Process(const std::shared_ptr<const JEvent> &event)
{

    using namespace fmt;
    using namespace std;
    using namespace minimodel;
    static bool said_once = false;

    // Get the inputs needed for this factory.

    std::vector<const minimodel::McGeneratedParticle *> gen_parts;

    bool smeared_taken;
    if(event->GetFactory<McGeneratedParticle>("smear", false)) {
        gen_parts = event->Get<McGeneratedParticle>("smear");
        smeared_taken = true;
    } else {
        gen_parts = event->Get<McGeneratedParticle>();
        smeared_taken = false;
    }

    if(!said_once) {
        said_once = true;
        print("JFactoryT<VMesonInputParticle> Smeared taken = {}", smeared_taken);
    }


    // text_event_record has just tokenized text from

    // Fill Original particles information
    std::vector<VMesonInputParticle *> vm_parts;
    for (auto gen_part : gen_parts) {
        if (!gen_part->is_stable) continue;      // skip not final state particles

        auto vm_part = new VMesonInputParticle();
        vm_part->pdg = gen_part->pdg;
        vm_part->p = ROOT::Math::PxPyPzMVector(gen_part->px, gen_part->py, gen_part->pz, gen_part->m);

        // auto p2 = ROOT::Math::PxPyPzEVector(gen_part->px, gen_part->py, gen_part->pz, gen_part->tot_e);
        // fmt::print("Mass compare: {:<10} {:<12} {:<12} {}\n", gen_part->pdg, gen_part->m, vm_part->p.mass(), p2.mass());
        // fmt::print("E    compare: {:<10} {:<12} {:<12} {}\n", gen_part->pdg, gen_part->tot_e, vm_part->p.energy(), p2.e());

        vm_part->vertex.SetXYZ(gen_part->vtx_x, gen_part->vtx_y, gen_part->vtx_z);
        vm_parts.push_back(vm_part);
    }

    Set(std::move(vm_parts));
}