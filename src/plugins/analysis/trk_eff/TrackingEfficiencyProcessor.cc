#include "TRandom.h"
#include "TLorentzVector.h"
#include "TVector3.h"

#include <fmt/core.h>

#include <JANA/JEventProcessor.h>
#include <JANA/JEvent.h>

#include <MinimalistModel/McFluxHit.h>
#include <MinimalistModel/McGeneratedParticle.h>


#include "TrackingEfficiencyProcessor.h"

using namespace fmt;

void TrackingEfficiencyProcessor::Init()
{
    // Ask service locator for parameter manager. We want to get this plugin parameters.
    auto pm = services_.Get<JParameterManager>();

    // Verbosity level. 0 = show nothing. 1 - show some. 2 - mad printer
    // SetDefaultParameter actually sets the parameter value from arguments if it is specified
    verbose_ = 0;
    pm->SetDefaultParameter("trk_eff:verbose", verbose_, "Plugin output level. 0-almost nothing, 1-some, 2-everything");


    // Setup histograms. We get current TFile from Service Locator
    histos_.Init(services_.Get<TFile>());

    // Print verbose 'hello'
    if (verbose_) {
        print("'trk_eff' plugin Init: \n");
        print("   Parameters:\n");
        print("     trk_eff:verbose:         {}\n", verbose_);
    }
}


void TrackingEfficiencyProcessor::Process(const std::shared_ptr<const JEvent> &event)
{
    ///< Called every event.

    bool smeared_taken;

    // Verbose output for each event
    if(verbose_ > 1) {
        print("\n====================================\n");
        print("trk_eff::Process( event number = {} ) \n", event->GetEventNumber());
    }

    // Process raw Geant4 events if available
    if(event->GetFactory<jleic::GeantPrimaryParticle>("", false)) {
        auto gen_parts = event->Get<jleic::GeantPrimaryParticle>();

        if(verbose_ > 1) {
            PrintGeantPrimaryParticles(gen_parts);
        }
    }
}


void TrackingEfficiencyProcessor::Finish()
{
    ///< Called after last event of last event source has been processed.
    fmt::print("TrackingEfficiencyProcessor::Finish(). Cleanup\n");
}

void TrackingEfficiencyProcessor::PrintGeantPrimaryParticles(const vector<const jleic::GeantPrimaryParticle *>& particles) {
    for(auto particle: particles) {
        print(" {} {:<10} \n", particle->id, particle->pdg);
    }
}

