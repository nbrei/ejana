//
// Created by Alexander on 17.07.2019.
//

#include "EventWriterProcessor.h"

#include "TRandom.h"

#include <fmt/core.h>

#include <JANA/JEventProcessor.h>
#include <JANA/JEvent.h>

#include <MinimalistModel/McFluxHit.h>
#include <MinimalistModel/McGeneratedParticle.h>
#include <io/beagle_reader/BeagleEventData.h>

using namespace fmt;
using namespace minimodel;

const int kMaxNTracks = 200;
uint64_t ntracks;
uint64_t event_number;
double   true_q2;
double true_x ;
double true_y ;
double true_w2;
double true_nu;
bool   has_true_dis;

uint64_t count[kMaxNTracks];
uint64_t id[kMaxNTracks];
uint64_t vtx_id[kMaxNTracks];    /// Generated vertex ID
int64_t  pdg[kMaxNTracks];       /// PDG value
uint64_t trk_id[kMaxNTracks];    /// ID of the related track
double   charge[kMaxNTracks];    /// no comments
double   dir_x[kMaxNTracks];     /// Direction unit vector component at vertex
double   dir_y[kMaxNTracks];
double   dir_z[kMaxNTracks];
double   p[kMaxNTracks];         /// total momentum
double   px[kMaxNTracks];        /// dir_x * p
double   py[kMaxNTracks];        /// dir_y * p
double   pz[kMaxNTracks];        /// dir_z * p
double   tot_e[kMaxNTracks];     /// total energy
double   t[kMaxNTracks];         /// time
double   pol_x[kMaxNTracks];     /// polarization
double   pol_y[kMaxNTracks];
double   pol_z[kMaxNTracks];


void EventWriterProcessor::Init()
{
    ///  Called once at program start.
    jout << "EventWriterProcessor: Init()" << std::endl;

    // initialize class that holds histograms and other root objects
    // 'services' is a service locator, we ask it for TFile
    root_out.init(services.Get<TFile>());
    root_out.tree->Branch("ntracks_b", &ntracks, "ntracks/l");
    root_out.tree->Branch("event_number_b", &event_number, "event_number/l");
    root_out.tree->Branch("true_q2_b", &true_q2, "true_q2/D");
    root_out.tree->Branch("true_x_b", &true_x , "true_x/D");
    root_out.tree->Branch("true_y_b", &true_y , "true_y/D");
    root_out.tree->Branch("true_w2_b", &true_w2, "true_w2/D");
    root_out.tree->Branch("true_nu_b", &true_nu, "true_nu/D");
    root_out.tree->Branch("has_true_dis_b", &has_true_dis, "has_true_dis/B");

    root_out.tree->Branch("count_b", count, "count[ntracks]/l");
    root_out.tree->Branch("id_b", id, "id[ntracks]/l");
    root_out.tree->Branch("vtx_id_b", vtx_id, "vtx_id[ntracks]/l");
    root_out.tree->Branch("pdg_b", pdg, "pdg[ntracks]/L");
    root_out.tree->Branch("trk_id_b", trk_id, "trk_id[ntracks]/l");
    root_out.tree->Branch("charge_b", charge, "charge[ntracks]/D");
    root_out.tree->Branch("dir_x_b", dir_x, "dir_x[ntracks]/D");
    root_out.tree->Branch("dir_y_b", dir_y, "dir_y[ntracks]/D");
    root_out.tree->Branch("dir_z_b", dir_z, "dir_z[ntracks]/D");
    root_out.tree->Branch("p_b", p, "p[ntracks]/D");
    root_out.tree->Branch("px_b", px, "px[ntracks]/D");
    root_out.tree->Branch("py_b", py, "py[ntracks]/D");
    root_out.tree->Branch("pz_b", pz, "pz[ntracks]/D");
    root_out.tree->Branch("tot_e_b", tot_e, "tot_e[ntracks]/D");
    root_out.tree->Branch("t_b", t, "t[ntracks]D");
    root_out.tree->Branch("pol_x_b", pol_x, "pol_x[ntracks]/D");
    root_out.tree->Branch("pol_y_b", pol_y, "pol_y[ntracks]/D");
    root_out.tree->Branch("pol_z_b", pol_z, "pol_z[ntracks]/D");

    // Ask service locator for parameter manager. We want to get this plugin parameters.
    auto pm = services.Get<JParameterManager>();

    // Verbosity level. 0 = show nothing. 1 - show some. 2 - mad printer
    // SetDefaultParameter actually sets the parameter value from arguments if it is specified
    verbose = 0;
    pm->SetDefaultParameter("event_writer:verbose", verbose, "Plugin output level. 0-almost nothing, 1-some");

}


void EventWriterProcessor::Process(const std::shared_ptr<const JEvent>& event)
{
    ///< Called every event.
    std::lock_guard<std::recursive_mutex> locker(root_out.lock);

    // Get the inputs needed for this factory.
    auto gen_particles = event->GetFactory<McGeneratedParticle>("smear")?   // If there is a smearing factory
                         event->Get<McGeneratedParticle>("smear"):
                         event->Get<McGeneratedParticle>();

    // curly braces for locker
    {

        if(event->GetFactory<ej::BeagleEventData>())
        {
            auto beagle_event = event->GetSingle<ej::BeagleEventData>();
            true_q2 = beagle_event->trueQ2;
            true_x  = beagle_event->truex;
            true_y  = beagle_event->truey;
            true_w2 = beagle_event->trueW2;
            true_nu = beagle_event->trueNu;
            has_true_dis = true;
        } else {
            true_q2 = 0;
            true_x  = 0;
            true_y  = 0;
            true_w2 = 0;
            true_nu = 0;
            has_true_dis = false;
        }
    }


    ntracks = (uint64_t) fmin(gen_particles.size(), kMaxNTracks);
    event_number = event->GetEventNumber();

    //*************************Loop over Generated particles *********************************
    int i = 0;
    for( auto particle: gen_particles) {

        if (i >= kMaxNTracks) break;

        std::lock_guard<std::recursive_mutex> locker(root_out.lock);


        count[i]  = particle->count;
        id[i]     = particle->id;
        vtx_id[i] = particle->vtx_id;
        pdg[i]    = particle->pdg;
        trk_id[i] = particle->trk_id;
        charge[i] = particle->charge;
        dir_x[i]  = particle->dir_x;
        dir_y[i]  = particle->dir_y;
        dir_z[i]  = particle->dir_z;
        p[i]      = particle->p;
        px[i]     = particle->px;
        py[i]     = particle->py;
        pz[i]     = particle->pz;
        tot_e[i]  = particle->tot_e;
        t[i]      = particle->time;
        pol_x[i]  = particle->pol_x;
        pol_y[i]  = particle->pol_y;
        pol_z[i]  = particle->pol_z;

        i++;
    }
    root_out.tree->Fill();
}


void EventWriterProcessor::Finish()
{
    ///< Called after last event of last event source has been processed.

    jout << "EventWriterProcessor::Finish(). Cleanup" << std::endl;
}
