#include "TRandom.h"
#include "TLorentzVector.h"
#include "TVector3.h"

#include <fmt/core.h>

#include <JANA/JEvent.h>

#include <MinimalistModel/McFluxHit.h>
#include <MinimalistModel/McGeneratedParticle.h>

#include "OpenCharmProcessor.h"
#include "Kin_Variables.h"

// TODO organize includes
#include <JANA/JEventProcessor.h>
#include <JANA/JEvent.h>
#include "eicsmear/erhic/VirtualParticle.h"
#include "eicsmear/smear/Acceptance.h"
#include "eicsmear/smear/Device.h"
#include "eicsmear/smear/Detector.h"
#include "eicsmear/smear/Smearer.h"
#include "eicsmear/smear/ParticleMCS.h"
#include "eicsmear/smear/PerfectID.h"
#include <eicsmear/smear/Smear.h>
#include <eicsmear/erhic/ParticleMC.h>
#include "Math/Vector4D.h"
#include <ejana/EServicePool.h>
#include <MinimalistModel/McGeneratedParticle.h>
#include <TClingRuntime.h>
#include <fmt/format.h>     // For format and print functions



// TODO repalce by some 'conventional' PDG info provider. Like TDatabasePDG
//==================================================================
  Double_t MassPI=0.139570,
         MassK=0.493677,
         MassP=0.93827,
         MassE=0.000511,
         MassMU=0.105658;
//==================================================================
 

using namespace std;
using namespace fmt;


struct OpenCharmRecord {
    double Q2_true;
    double Q2_smeared;
    double Q2_trkng;
    double Xel,Yel, Q2el;
   // double Xtrue,Ytrue, Q2true;
    double Xgen, Ygen, Q2gen;
    double Eth;
    double Ee;
    double p;
    double pt;
};

static OpenCharmRecord vmrec;


void OpenCharmProcessor::Init()
{
    ///  Called once at program start.
    jout << "OpenCharmProcessor: Init()" << endl;

    // initialize class that holds histograms and other root objects
    // 'services' is a service locator, we ask it for TFile
    root_out.init(services.Get<TFile>());
    root_out.tree_rec_e->Branch("Q2_true", &vmrec.Q2_true);
    root_out.tree_rec_e->Branch("Q2_smeared", &vmrec.Q2_smeared);
    root_out.tree_rec_e->Branch("Q2_trkng", &vmrec.Q2_trkng);
    root_out.tree_rec_e->Branch("Xel", &vmrec.Xel);
    root_out.tree_rec_e->Branch("Yel", &vmrec.Yel);
    root_out.tree_rec_e->Branch("Q2el", &vmrec.Q2el);
    root_out.tree_rec_e->Branch("Xgen", &vmrec.Xgen);
    root_out.tree_rec_e->Branch("Ygen", &vmrec.Ygen);
    root_out.tree_rec_e->Branch("Q2gen", &vmrec.Q2gen);
    root_out.tree_rec_e->Branch("Eth", &vmrec.Eth);
    root_out.tree_rec_e->Branch("Ee", &vmrec.Ee);
    root_out.tree_rec_e->Branch("p", &vmrec.p);
    root_out.tree_rec_e->Branch("pt", &vmrec.pt);
    //

    // Ask service locator for parameter manager. We want to get this plugin parameters.
    auto pm = services.Get<JParameterManager>();

    // Verbosity level. 0 = show nothing. 1 - show some. 2 - mad printer
    // SetDefaultParameter actually sets the parameter value from arguments if it is specified
    verbose = 0;
    pm->SetDefaultParameter("open_charm:verbose", verbose, "Plugin output level. 0-almost nothing, 1-some, 2-everything");

    smearing = 0;
    pm->SetDefaultParameter("open_charm:smearing", smearing, "Particle smearing 0-true MC, 1-smearing, 2-reconstruction");

    // Beams energies
    pm->SetDefaultParameter("open_charm:e_beam_energy", e_beam_energy, "Energy of colliding electron beam");
    pm->SetDefaultParameter("open_charm:ion_beam_energy", ion_beam_energy, "Energy of colliding ion beam");
    if(verbose) {
        print("Parameters:\n");
        print("  open_charm:verbose:         {0}{0}\n", verbose);
        print("  open_charm:e_beam_energy:   {}\n", e_beam_energy);
        print("  open_charm:ion_beam_energy: {}\n", ion_beam_energy);
    }

    auto str = format("Hellop {}", 42);
}


void OpenCharmProcessor::Process(const std::shared_ptr<const JEvent>& event)
{
    ///< Called every event.
    using namespace fmt;
    using namespace std;
    using namespace minimodel;
    std::lock_guard<std::recursive_mutex> locker(root_out.lock);

    double Xtrue,Ytrue, Q2true;

    // >oO debug printing
    if(event->GetEventNumber()%1000==0 && verbose > 1) {
        print("\n--------- EVENT {} --------- \n", event->GetEventNumber());
    }

    root_out.hEventN->Fill(event->GetEventNumber());
    // Get the inputs needed for this factory.
    auto particles = event->Get<OpenCharmInputParticle>();

    //   --- calculate kinematic variables using electron only method -------------
    for( auto electron: particles) {


        if (abs(electron->pdg) == 11 && electron->charge==-1) {
            if (verbose >= 2) { print("This is a place to check kinematics X,Q2 \n"); }
            double Xel, Yel, Q2el;
            double E_el = electron->p.E();
            double Theta_el = electron->p.Theta();

            double PISL[4]={0.,0., e_beam_energy, sqrt(e_beam_energy*e_beam_energy+MassE*MassE)};
            double PISP[4]={0.,0., -ion_beam_energy,sqrt(ion_beam_energy*ion_beam_energy+MassP*MassP)};

            double PFSL[4]={ electron->p.px(), electron->p.py(),electron->p.pz(), sqrt(electron->p.E()*electron->p.E()+MassE*MassE)};

    //        GEN_XYQ2(PFSL,PISL,PISP,Xtrue, Ytrue, Q2true);
     //       root_out.h2_XQ2_true->Fill(log10(Xtrue), log10(Q2true));

            GEN_El_XYQ2(E_el, Theta_el, e_beam_energy, ion_beam_energy, Xel, Yel, Q2el);
            root_out.h2_XQ2_em->Fill(log10(Xel), log10(Q2el));

            if (verbose >= 2){
                print("Charm:Ee {:<11McGeneratedParticle} Th {:<11}  (Xtrue {:<11} Xel {:<11}) ( Ytrue {:<11}  Yel {:<11}) Q2true {:<11} Q2el {:<11} \n",
                        electron->p.E(), electron->p.Theta(), Xtrue,Xel, Ytrue,Yel, Q2true,Q2el);
            }


/*
            root_out.h2_Xem_diff->Fill(log10(Xtrue),(Xel-Xtrue)/Xtrue);
            root_out.h2_Yem_diff->Fill(log10(Ytrue),(Yel-Ytrue)/Ytrue);
            root_out.h2_Q2em_diff->Fill(log10(Q2true),(Q2el-Q2true)/Q2true);



            root_out.h2_Xem_diff->Fill(log10(Xtrue),(Xel-Xtrue)/Xtrue);
            root_out.h2_Yem_diff->Fill(log10(Ytrue),(Yel-Ytrue)/Ytrue);
            root_out.h2_Q2em_diff->Fill(log10(Q2true),(Q2el-Q2true)/Q2true);

            root_out.h2_Q2_em_true->Fill(log10(Q2true), log10(Q2el));
*/

            if (Yel > 0.05 && Yel < 0.95) {
                root_out.h2_XQ2_em_cuts->Fill(log10(Xel), log10(Q2el));
            }
            //     GEN_XYQ2_4(electron->p(), Xtrue,Ytrue,Q2true);
            //        print("Charm:Ee {:<11} Th {:<11} Xel {:<11} Yel {:<11} Q2el {:<11} \n",electron->p.E(),electron->p.Theta(),Xel,Yel,Q2el);

        }
    }


     //*************************Loop over Generated particles *********************************
    //------------------------------------------------------------------------------

	for( auto particle1: particles) {  //----- loop -I
        std::lock_guard<std::recursive_mutex> locker(root_out.lock);


        if(verbose >= 2) {
            print("OCharm: particle {:<5} px: {:<11} py: {:<11} pz: {:<11} pt: {:<11} charge: {:<5} \n",
                    particle1->pdg, particle1->p.px(), particle1->p.py(), particle1->p.pz(), particle1->p.pt(),particle1->charge);
        }


        if(particle1->charge==0 ) continue; // request good charge
        if(particle1->p.pt()<0.1) continue;   // request min pt
        if(abs(particle1->pdg)!= 321) continue; // Kaons only

        TLorentzVector ka; ka.SetXYZM(particle1->p.px(),particle1->p.py(),particle1->p.pz(),MassK);

        //-------- displaced vertex of Kaons -----------------
        root_out.hD0_vtxK->Fill(particle1->vertex.Mag());
            // print(" generated particles PID {} px={} py={} pz={}\\\n", particle->pid, particle->px,particle->py,particle->pz);
        //------------------------------------------------------------------------------
        for( auto particle2: particles) {  //----- loop -J
            if (verbose >= 2) {
                print("OCharm2: particle1 {:<5},  particle2 {:<5},  \n",
                      particle1->id, particle2->id);
            }

            if (particle1->id == particle2->id) continue; // request good charge
            if (particle1->charge * particle2->charge >= 0) continue; // request good charge
            if (particle2->p.pt() < 0.1) continue;   // request min pt
            if (abs(particle2->pdg) != 211) continue; // Pions only

            TLorentzVector pi;
            pi.SetXYZM(particle2->p.px(), particle2->p.py(), particle2->p.pz(), MassPI);
            //-------------VERTEX cut ---------------------------
            TVector3 dvtxD0 = particle1->vertex - particle2->vertex;
            root_out.hD0_vtx->Fill(dvtxD0.Mag());
            if (verbose >= 2)
                print("CharmD0: diff vertex {:<11}, K vtx mag {:<11} PI vtx mag {:<11} \n", dvtxD0.Mag(),
                      particle1->vertex.Mag(), particle2->vertex.Mag());
            if (particle1->vertex.Mag() < 100. || particle2->vertex.Mag() < 100.) continue;
            //   if(dvtxD0.Mag()>10.) continue;

            //---------------Inv Mass for D0----------------------
            TLorentzVector D0 = pi + ka;

            root_out.hD0_mass->Fill(D0.M());
            //root_out.hD0_mass->Fill(gRandom->Gaus(D0.M(),0.01));}
            if(D0.M()< 1.79 || D0.M()> 1.93 ) {
                root_out.hD0_KaonPt->Fill(particle1->p.pt());
                root_out.hD0_KaonPtot->Fill(particle1->p.P());
                root_out.hD0_KaonPvsEta->Fill(particle1->p.Eta(),particle1->p.P());
                root_out.hD0_pt->Fill(D0.Perp());
            }
            //------------------------------------------------------------------------------
            for (auto particle3: particles) {  //----- loop -K
                if (particle3->id == particle1->id || particle3->id == particle2->id) continue; // request good charge
                if (particle3->id > particle1->id) continue; // request good charge
                if (particle1->charge * particle3->charge >= 0) continue; // request good charge
                if (abs(particle3->pdg) != 211) continue; // accept only pions.

                //-------------- vertex cut------------------------
                TVector3 Dst_vtx2 = particle1->vertex - particle3->vertex;
                root_out.hDst_vtx->Fill(Dst_vtx2.Mag());
                //   if (dvtx2.Mag()>100) continue;

                // ------------ inv mass D* and D+ (K_pi_pi)------------------------
                TLorentzVector pi_slow;
                pi_slow.SetXYZM(particle3->p.px(), particle3->p.py(), particle3->p.pz(), MassPI);

                TLorentzVector Dstar = D0 + pi_slow;
                Double_t DIFF = Dstar.M() - D0.M();
                // ------------ inv mass D+ ------------------------
                 if(particle3->vertex.Mag()>100.){ root_out.hDpl_mass->Fill(Dstar.M());}
                // ------------ inv mass D* -D0 ------------------------
                //if(Dst_vtx2.Mag()<60){root_out.hDst_dm->Fill(DIFF);}
                root_out.hDst_dm->Fill(DIFF);
                //root_out.hDpl_mass->Fill(gRandom->Gaus(Dstar.M(),0.001));
                //root_out.hDst_dm->Fill(gRandom->Gaus(DIFF,0.001));

                root_out.hDst_pt->Fill(Dstar.Perp());
                root_out.hDst_D0->Fill(gRandom->Gaus(D0.M(),0.01),gRandom->Gaus(Dstar.M(),0.01));

                //---- set   mass cut  1.79< D0< 1.93
                if(D0.M()< 1.79 || D0.M()> 1.93 ) continue;
                root_out.hDst_dm_mcut->Fill(DIFF);
                //root_out.hDst_dm_mcut->Fill(gRandom->Gaus(DIFF,0.001));
                root_out.hDst_pt_mcut->Fill(Dstar.Perp());



            } // -- end loop -K
            //------------------------------------------------------------------------------
        }    // -- end loop -J
        //------------------------------------------------------------------------------

    } // ---- end loop -I
}


void OpenCharmProcessor::Finish() {
    ///< Called after last event of last event source has been processed.
    jout << "OpenCharmProcessor::Finish(). Cleanup" << endl;
}



template <>
void JFactoryT<OpenCharmInputParticle>::Process(const std::shared_ptr<const JEvent>& event) {

    using namespace fmt;
    using namespace std;
    using namespace minimodel;

    // Get the inputs needed for this factory.
    auto gen_parts = event->GetFactory<McGeneratedParticle>("smear")?   // If there is a smearing factory
                     event->Get<McGeneratedParticle>("smear"):
                     event->Get<McGeneratedParticle>();


    // text_event_record has just tokenized text from

    // Fill Original particles information
    std::vector<OpenCharmInputParticle *> oc_particles;
    for (auto gen_part : gen_parts) {
        auto oc_particle = new OpenCharmInputParticle();
        oc_particle->pdg = gen_part->pdg;

        oc_particle->charge = gen_part->charge;
        oc_particle->id = gen_part->id;
//        oc_particle->p.SetPxPyPzE(gen_part->px, gen_part->py, gen_part->pz, gen_part->tot_e);
        oc_particle->p.SetPxPyPzE(gen_part->px, gen_part->py, -gen_part->pz, gen_part->tot_e);
        // ---- convert to um ------
        oc_particle->vertex.SetXYZ(gen_part->vtx_x * 1000., gen_part->vtx_y * 1000., gen_part->vtx_z * 1000.);
        oc_particles.push_back(oc_particle);
    }

    Set(std::move(oc_particles));
}
