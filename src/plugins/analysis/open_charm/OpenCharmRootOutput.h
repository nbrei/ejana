#ifndef OPEN_CHARM_ROOT_OUTPUT_HEADER
#define OPEN_CHARM_ROOT_OUTPUT_HEADER

#include <TFile.h>
#include <TDirectory.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TApplication.h>

class OpenCharmRootOutput
{
public:
    void init(TFile *file)
    {
        // 'locker' locks mutex so other threads can't interfere with TFile doing its job
        std::lock_guard<std::recursive_mutex> locker(lock);        

        // create a subdirectory "hist_dir" in this file
        dir_main = file->mkdir("opencharm");
        file->cd();         // Just in case some other root file is the main TDirectory now

        // TTree with recoiled electron
        tree_rec_e = new TTree("rec_e", "a Tree with vect");
        tree_rec_e->SetDirectory(dir_main);

        //------------------------------------ High-x  -----------------------------------------------------
        const int XBINS_h=19, Q2BINS_h=16;
        double xEdges_h[XBINS_h + 1] =
                {-3.8, -3.6, -3.4, -3.2, -3.,
                 -2.8, -2.6, -2.4, -2.2, -2.,
                 -1.8, -1.6, -1.4, -1.2, -1.,
                 -0.8, -0.6, -0.4, -0.2, 0.};


        Double_t Q2Edges_h[Q2BINS_h + 1] =
                {0.,   0.25,  0.5,  0.75, 1.,
                 1.25, 1.5,   1.75, 2.,   2.25,
                 2.5,  2.75,  3.,   3.25, 3.5,
                 3.75, 4.};
/*
        h2_XQ2_true = new TH2I("h2_XQ2_true","True X,Q2 ", XBINS_h,xEdges_h, Q2BINS_h, Q2Edges_h);
        h2_XQ2_true->SetDirectory(dir_main);
        h2_XQ2_true->GetXaxis()->SetTitle("log(x)");
        h2_XQ2_true->GetYaxis()->SetTitle("log(Q^{2})");


        hQ2_true = new TH1D("hQ2_true","True Q2 ",100, 0,5000);
        hQ2_true->SetDirectory(dir_main);
        hQ2_true->GetXaxis()->SetTitle("Q2_{BJ}");

        hX_true = new TH1D("hX_true","True X ",100, 0.,1.);
        hX_true->SetDirectory(dir_main);
        hX_true->GetXaxis()->SetTitle("X_{BJ}");

        hY_true = new TH1D("hY_true","True Y ",100, 0.,1.);
        hY_true->SetDirectory(dir_main);
        hY_true->GetXaxis()->SetTitle("X_{BJ}");
*/
        //----- electron Method --------------------
        h2_XQ2_em = new TH2I("h2_XQ2_em","Em X,Q2 ", XBINS_h,xEdges_h, Q2BINS_h, Q2Edges_h);
        h2_XQ2_em->SetDirectory(dir_main);
        h2_XQ2_em->GetXaxis()->SetTitle("log(x)");
        h2_XQ2_em->GetYaxis()->SetTitle("log(Q^{2})");

        h2_XQ2_em_cuts = new TH2I("h2_XQ2_em_cuts ","El X,Q2 cuts ", XBINS_h,xEdges_h, Q2BINS_h, Q2Edges_h);
        h2_XQ2_em_cuts->SetDirectory(dir_main);
        h2_XQ2_em_cuts->GetXaxis()->SetTitle("log(x)");
        h2_XQ2_em_cuts->GetYaxis()->SetTitle("log(Q^{2})");

        /*
        h2_Q2_em_true = new TH2I("h2_Q2_em_true","Em vs True (Q2em ,Q2true) ", Q2BINS_h, Q2Edges_h, Q2BINS_h, Q2Edges_h);
        h2_Q2_em_true->SetDirectory(dir_main);
        h2_Q2_em_true->GetXaxis()->SetTitle("True log(Q2)"); h2_XQ2_em->GetYaxis()->SetTitle("Em log(Q^{2})");


        int diff_bin_em=100;
        h2_MxY_em  = new TH2I("h2_MxY_em","hMxY_em",   diff_bin_em,0. , 100., 100, 0. , 1.);
        h2_MxY_em ->SetDirectory(dir_main);

        h2_Xem_diff  = new TH2F("h2_Xem_diff" ,"(x_{em}-x_{true})/x_{true} vs x_{true}"    , diff_bin_em, -2.8 , 0. ,  diff_bin_em, -1.   ,1.  );
        h2_Xem_diff->GetXaxis()->SetTitle("log(x_{true})"); h2_Xem_diff->GetYaxis()->SetTitle("(x_{em}-x_{true})/x_{true}");
        h2_Xem_diff->SetDirectory(dir_main);
        h2_Yem_diff  = new TH2F("h2_Yem_diff" ,"(y_{em}-y_{true})/y_{true} vs y_{true}"    , diff_bin_em, -2.8 , 0. ,  diff_bin_em, -1.   ,1.  );
        h2_Yem_diff->GetXaxis()->SetTitle("log(y_{true})"); h2_Yem_diff->GetYaxis()->SetTitle("(y_{em}-y_{true})/y_{true}");
        h2_Yem_diff->SetDirectory(dir_main);
        h2_Q2em_diff  = new TH2F("h2_Q2em_diff" ,"(Q^{2}_{em}-Q^{2}_{true})/Q^{2}_{true} vs Q^{2}_{true}"    ,  diff_bin_em, -2. , 4. ,  diff_bin_em, -1.   , 1.  );
        h2_Q2em_diff->GetXaxis()->SetTitle("log(Q^{2}_{true})"); h2_Q2em_diff->GetYaxis()->SetTitle("(Q^{2}_{em}-Q^{2}_{true})/Q^{2}_{true}");
        h2_Q2em_diff->SetDirectory(dir_main);


        h_x_diff_em  = new TH1F("h_x_diff_em" ,"x_{em}-x_{true})/x_{true}  "     , diff_bin_em, -2.8 , 0.   );
        h_x_diff_em->GetXaxis()->SetTitle("log(x_{true})");h_x_diff_em->GetYaxis()->SetTitle("(x_{em}-x_{true})/x_{true}");
        h_x_diff_em->SetDirectory(dir_main);
        h_y_diff_em  = new TH1F("h_y_diff_em" ,"y_{em}-y_{true})/y_{true}  "     ,  diff_bin_em, -2.8 , 0.   );
        h_y_diff_em->GetXaxis()->SetTitle("log(y_{true})");h_y_diff_em->GetYaxis()->SetTitle("(y_{em}-y_{true})/y_{true}");
        h_y_diff_em->SetDirectory(dir_main);
        h_q2_diff_em  = new TH1F("h1q2_diff_em" ," Q^{2}_{em}-Q^{2}_{true})/Q^{2}_{true}  "     ,  diff_bin_em, -2. , 4.   );
        h_q2_diff_em->GetXaxis()->SetTitle("log(Q^{2}_{true})");h_q2_diff_em->GetYaxis()->SetTitle("(Q^{2}_{em}-Q^{2}_{true})/Q^{2}_{true}");
        h_q2_diff_em->SetDirectory(dir_main);
         */
        //----------------------------------------

/*
        hXQ2_smear = new TH2I("hXQ2_smear","Smear X, Q2 ", XBINS_h, xEdges_h, Q2BINS_h, Q2Edges_h);
        hXQ2_smear->SetDirectory(dir_main);
        hXQ2_smear->GetXaxis()->SetTitle("log(x)");
        hXQ2_smear->GetYaxis()->SetTitle("log(Q^{2})");

        hpt_eth = new TH2D("Pt vs Pz/Ptot", "Pt", 100,1,100, 100,1,100);
        hpt_eth->SetDirectory(dir_main);
*/

        hD0_mass = new TH1D("D0_mass", "D0 mass", 100,1.45,2.2);
        hD0_mass->GetXaxis()->SetTitle("m_{K#pi} [GeV/c^{2}]");
        hD0_mass->SetDirectory(dir_main);
        hD0_vtx= new TH1D("D0_vtx", "D0 vtx", 100,0.,300.);
        hD0_vtx->GetXaxis()->SetTitle("vertex [#mum]");
        hD0_vtx->SetDirectory(dir_main);

        hD0_pt = new TH1D("D0_pt", "D0 pt", 100,0.,5.);
        hD0_pt->GetXaxis()->SetTitle("p_{T} [GeV]");
        hD0_pt->SetDirectory(dir_main);

        hD0_KaonPt = new TH1D("hD0_KaonPt", "Kaons from D0 (pt)", 100,0.,5.);
        hD0_KaonPt->GetXaxis()->SetTitle("p_{T} [GeV]");
        hD0_KaonPt->SetDirectory(dir_main);

        hD0_KaonPtot = new TH1D("hD0_KaonPtot", "Kaons from D0 (ptot)", 100,0.,5.);
        hD0_KaonPtot->GetXaxis()->SetTitle("p [GeV]");
        hD0_KaonPtot->SetDirectory(dir_main);

        hD0_KaonPvsEta = new TH2D("hD0_KaonPvsEta", " Kaons from D0 ptot vs eta", 100,-5.,5., 100,0.,20.);
        hD0_KaonPvsEta->GetXaxis()->SetTitle("#eta ");
        hD0_KaonPvsEta->GetYaxis()->SetTitle(" p_{tot}[GeV]");
        hD0_KaonPvsEta->SetDirectory(dir_main);

        hD0_vtxK = new TH1D("D0_vtx_K", "D0 VTX K ", 100,0.,1000.);
        hD0_vtxK->GetXaxis()->SetTitle("vertex of K [#mum]");
        hD0_vtxK->SetDirectory(dir_main);

        hDst_vtx= new TH1D("Dst_vtx", "Dst vtx", 100,0.,300.);
        hDst_vtx->GetXaxis()->SetTitle("vertex [#mum]");
        hDst_vtx->SetDirectory(dir_main);

        hDst_dm = new TH1D("Dst_dm", "D*-D0 mass", 50,0.139,0.17);
        hDst_dm->GetXaxis()->SetTitle("m_{K#pi#pi} - m_{K#pi} [GeV/c^{2}]");
        hDst_dm->SetDirectory(dir_main);

        hDpl_mass = new TH1D("Dpl_mass", "Dpl mass", 100,1.5,2.4 );
        hDpl_mass->GetXaxis()->SetTitle("m_{K#pi#pi} [GeV/c^{2}]");
        hDpl_mass->SetDirectory(dir_main);

        hDst_D0 = new TH2D("Dst_m_vs_D0_m", "D* mass  vs D0 mass", 100,1.4,2.5, 100,1.4,2.5);
        hDst_D0->GetYaxis()->SetTitle("m_{K#pi#pi} [GeV]");hDst_D0->GetXaxis()->SetTitle(" m_{K#pi} [GeV]");
        hDst_D0->SetDirectory(dir_main);

        hDst_pt = new TH1D("Dst_pt", "Dst pt", 100,0.,5.);
        hDst_pt->GetXaxis()->SetTitle("p_{T} [GeV]");
        hDst_pt->SetDirectory(dir_main);

        hDst_dm_mcut = new TH1D("Dst_dm_mcut", "D*-D0 mass (D0 mass cut)", 100,0.139,0.17);
        hDst_dm_mcut->GetXaxis()->SetTitle("m_{K#pi#pi}-m_{D0} [GeV/c^{2}]");
        hDst_dm_mcut->SetDirectory(dir_main);

        hDst_pt_mcut = new TH1D("Dst_pt_mcut", "Dst pt (D0 mass cut)", 100,0.,5.);
        hDst_pt_mcut->GetXaxis()->SetTitle("p_{T} [GeV]");
        hDst_pt_mcut->SetDirectory(dir_main);

        hEventN = new TH1D("EventN", "Event N", 100,0.,1000.);
        hEventN->GetXaxis()->SetTitle("Event Number");

        hEventN->SetDirectory(dir_main);
    }

    // ---- kinematic variables---
    TH1D *hD0_mass, *hD0_vtx, *hD0_KaonPt,*hD0_KaonPtot,*hD0_pt,*hD0_vtxK;
    TH2D *hD0_KaonPvsEta;
    TH1D *hDpl_mass, *hDst_vtx, *hDst_pt,*hDst_dm, *hDst_pt_mcut,*hDst_dm_mcut;
    TH2D *hDst_D0;
    TH2I *h2_XQ2_true,  *hXQ2_smear;
    TH2I *h2_XQ2_em, *h2_Q2_em_true,*h2_XQ2_em_cuts, *h2_MxY_em;
    TH2F  *h2_Xem_diff,*h2_Yem_diff,*h2_Q2em_diff;
    TH1F  *h_x_diff_em,*h_y_diff_em,*h_q2_diff_em;
    TH1D *hX_true, *hQ2_true, *hY_true;
    TH1D *hEventN;
    std::recursive_mutex lock;

    TTree * tree_rec_e;     // Tree to store electron related data
    TH2D *hpt_eth;

private:

    TDirectory* dir_main;   // Main TDirectory for Plugin histograms and data

};

#endif // OPEN_CHARM_ROOT_OUTPUT_HEADER