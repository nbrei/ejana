// ejana stands for
// EIC Jana ROOT(Cern) flavoried application

#include <dlfcn.h>
#include <thread>
#include <atomic>
#include <TROOT.h>
#include <TSysEvtHandler.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TCanvas.h>

#include <JANA/JApplication.h>

#include <ejana/EJanaRootApplication.h>

#include <unistd.h>
#include <TBrowser.h>
#include <JANA/Status/JVersion.h>

void PrintUsage();

  /// System event loop.
static std::atomic<bool> root_gui_is_running(false);
static std::atomic<bool> command_stop_root_gui(false);

void ParseCommandLineArguments(int &narg, char *argv[],
							   JParameterManager* par_man,
							   std::vector<string>* eventSources);

void RootGuiEventLoop()
{
    // TODO run RootGuiEventLoop in parallel with JANA.Run
	root_gui_is_running = true;
    gApplication = new TApplication("ejana", nullptr , 0);
    TBrowser tb;
    tb.Draw();

    try {
		// https://root.cern/doc/master/TSystem_8cxx_source.html
		// Look at TSystem::Run() there 
        while (!command_stop_root_gui) {
             gApplication->StartIdleing();
			 gSystem->InnerLoop();
             gApplication->StopIdleing();
			 //gSystem->ProcessEvents();
			 std::this_thread::yield();
        }
    }
    catch (std::exception& exc) {
		
       throw;
    }
    catch (const char *str) {
       printf("%s\n", str);
    }
    // handle every exception
    catch (...) {
       Warning("Run", "handle uncaught exception, terminating");
	   throw;
    }

	root_gui_is_running = false;
}

//-----------
// main
//-----------
int main(int narg, char *argv[])
{
	// Parse the command line
	std::vector<std::string> all_args;

	auto params = new JParameterManager;
	auto eventSources = new std::vector<string>;
	ParseCommandLineArguments(narg, argv, params, eventSources);


	if (narg <= 1) {
		return 0;   // No arguments provided. Exit. (usage printed in ParseCommandLineArguments)
	}
	else {
		all_args.assign(argv + 1, argv + narg);
	}


	char buffer[1024];
	char *answer = getcwd(buffer, sizeof(buffer));
	string s_cwd;
	if (answer)
	{
		s_cwd = answer;
	}
	std::cout<<"getcwd: "<<s_cwd<<std::endl;

	//Set the default output file. Won't be used if another output file is specified by user
	std::string default_out_name = "output.root";
	params->SetDefaultParameter("output", default_out_name, "ROOT output file");

	// Instantiate eJana application
	ej::EJanaRootApplication app(params);

	for(const auto& eventSourcePath : *eventSources) {
	    app.Add(eventSourcePath);
	}

	// Get parameter manager. 
	// (It is responsible for getting user defined parameters from command line or config file)
	auto pm = app.GetJParameterManager();

    // Create gROOT Cern.Root application
    gApplication = new TApplication("ejana", nullptr , nullptr);

	// Call ROOT::EnableThreadSafety if not explicitly denied by a user
	bool root_enable_ts = true;
	pm->SetDefaultParameter("ROOT:EnableThreadSafety", root_enable_ts, "Calls ROOT::EnableThreadSafety() if set 'true'");
	if(root_enable_ts) {
		ROOT::EnableThreadSafety();
	}

	// Run though all events, calling our event processor's methods
	app.Run();

	if( app.GetExitCode() )
	{
		std::cerr << "Exit code: " << app.GetExitCode() << std::endl;
	}

    bool root_run_app = false;
    pm->SetDefaultParameter("ROOT:StartGui", root_run_app, "Starts TApplication::Run() after eJANA finish");
    if(root_run_app) {
        //root_thread = std::thread(RootGuiEventLoop);
        TBrowser tb;
        tb.Draw();
        gApplication->Run();
    }

	return app.GetExitCode();
}


/// Prints application usage
//--------------------------
void PrintUsage()
{
	using namespace std;

	// TODO use JApplication::Usage function to print JANA options
	// the problem with it is that it is ridiculously not static and requires JApplication object
	// One should fix JANA first and then use it here

	cout <<
	"Usage:\n"
	"       ejana [options] source1 source2 ...\n"
	"Process events from various data sources (e.g. from files)\n"
	"This will create a ROOT file that plugins or debug histos\n"
	"can write into.\n"
	"\n"
	"Options:\n"
	"  --nthreads=X             Launch X processing threads\n"
	"  --plugin=plugin_name     Attach the plug-in named \"plugin_name\"\n"
	"  --so=shared_obj          Attach a plug-in with filename \"shared_obj\"\n"
	"  --sodir=shared_dir       Add the directory \"shared_dir\" to search list\n"
    "  --nevents=X              Process only N events from the event source\n"
    "  --nskip=X                Skip first N events from the event source\n"
	"  -Pkey=value              Set configuration parameter \"key\" to \"value\"\n"
	"  -Pprint                  Print all configuration params\n"
	<< endl;
}

//-----------
// ParseCommandLineArguments
//-----------
// Other args we might want:
// --config, --dumpcalibrations, --dumpconfig, --listconfig, --resourcereport

void ParseCommandLineArguments(int &narg, char *argv[],
							   JParameterManager* par_man,
							   std::vector<string>* eventSources)
{
	using namespace std;

	if(narg<=1) {
		PrintUsage();
		return;
	}

	for(int i=1;i<narg;i++){

		if(argv[i][0] != '-') {
			string arg = argv[i];
			eventSources->push_back(arg);
			// TODO: Consider making eventSources be a normal parameter
			continue;
		}

		string name, tag;
		size_t pos;
		string arg = argv[i];
		switch(argv[i][1]){

			case 'P':
				pos = arg.find('=');
				if( (pos!= string::npos) && (pos>2) ){
					string key = arg.substr(2, pos-2);
					string val = arg.substr(pos+1);
					par_man->SetParameter(key, val);
				}else{
					cout << " Bad parameter argument '" << arg
						 << "': Expected format -Pkey=value" << endl;
				}
				break;

			case 'h':
				PrintUsage();
				break;

			case 'v':
				cout<<"          JANA version: " << JVersion::GetVersion() << endl;
				cout<<"        JANA ID string: " << JVersion::GetIDstring() << endl;
				cout<<"     JANA git revision: " << JVersion::GetRevision() << endl;
				cout<<"JANA last changed date: " << JVersion::GetDate() << endl;
				cout<<"           JANA source: " << JVersion::GetSource() << endl;
				exit(0);

			case 'D':
				name = &argv[i][2];
				tag = "";
				pos = name.rfind(':', name.size()-1);
				if(pos != (unsigned int)string::npos){
					tag = name.substr(pos+1,name.size());
					name.erase(pos);
				}
				// autoactivate[name] = tag;
				break;
			case 'A':
//				ACTIVATE_ALL = 1;
				break;
			case '-':
				if(string(argv[i])=="--help"){
					PrintUsage();
					break;
				}
				break;
		}
	}
}
