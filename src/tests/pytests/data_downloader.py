import urllib.request
...

# Download the file from `url` and save it locally under `file_name`:
url = 'https://gitlab.com/eic/epw/raw/master/data/beagle_eD.txt'
file_name = 'beagle_eD.txt'
urllib.request.urlretrieve(url, file_name)
