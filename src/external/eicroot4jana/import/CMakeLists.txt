
include_directories(
${PROJECT_SOURCE_DIR}/fairroot
${PROJECT_SOURCE_DIR}/eicroot
${PROJECT_SOURCE_DIR}/import

# eic-smear headers;
${CMAKE_INSTALL_PREFIX}/include
)

SET(IMPORT_LIB import)
# FIXME: unify this line and INSTALL(FILES) below in a single cmake module call;		
SET(IMPORT_ROOTMAP ${CMAKE_CURRENT_BINARY_DIR}/libimport_rdict.pcm ${CMAKE_CURRENT_BINARY_DIR}/libimport.rootmap)

# --------------------------------------------------------

SET( IMPORT_SRC
EicProtoGenerator.cxx
EicBoxGenerator.cxx
EicEventGenerator.cxx

G__import.cxx
)

# In one line, no stupid extra SET() calls; FIXME: use eicroot.cmake;
ROOT_GENERATE_DICTIONARY(G__import EicProtoGenerator.h EicBoxGenerator.h EicEventGenerator.h LINKDEF importLinkDef.h)

ADD_LIBRARY( ${IMPORT_LIB} SHARED ${IMPORT_SRC} )
TARGET_LINK_LIBRARIES(${IMPORT_LIB} ${ROOT_LIBRARIES} fairroot eicroot ${CMAKE_INSTALL_PREFIX}/lib/libeicsmear.so )

INSTALL(TARGETS ${IMPORT_LIB}     DESTINATION lib)
INSTALL(FILES   ${IMPORT_ROOTMAP} DESTINATION lib)
