
include_directories(
${PROJECT_SOURCE_DIR}/fairroot
${PROJECT_SOURCE_DIR}/eicroot
)

SET(EICROOT_LIB eicroot)
# FIXME: unify this line and INSTALL(FILES) below in a single cmake module call;		
SET(EICROOT_ROOTMAP ${CMAKE_CURRENT_BINARY_DIR}/libeicroot_rdict.pcm ${CMAKE_CURRENT_BINARY_DIR}/libeicroot.rootmap)

# --------------------------------------------------------

SET( EICROOT_SRC
EicGeoMap.cxx
EicDetName.cxx
EicNamePatternHub.cxx 
EicGeoParData.cxx

EicMoCaPoint.cxx

G__eicroot.cxx
)

# In one line, no stupid extra SET() calls; FIXME: use eicroot.cmake;
ROOT_GENERATE_DICTIONARY(G__eicroot EicGeoMap.h EicDetName.h EicNamePatternHub.h EicGeoParData.h FairMCPoint.h EicMoCaPoint.h EicUnits.h LINKDEF eicrootLinkDef.h)

ADD_LIBRARY( ${EICROOT_LIB} SHARED ${EICROOT_SRC} )
TARGET_LINK_LIBRARIES(${EICROOT_LIB} ${ROOT_LIBRARIES} Geom fairroot)

INSTALL(TARGETS ${EICROOT_LIB}     DESTINATION lib)
INSTALL(FILES   ${EICROOT_ROOTMAP} DESTINATION lib)
