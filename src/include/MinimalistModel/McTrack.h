/**
 * Monte Carlo true information about tracks
 */
#ifndef MC_MINIMODEL_TRACK_HH
#define MC_MINIMODEL_TRACK_HH

#include <vector>
#include <JANA/JObject.h>
#include <ejana/EStringHelpers.h>

namespace minimodel
{
	class McTrack : public JObject
	{
	public:

		McTrack() {}

		uint64_t id;
		uint64_t pdg;      /// PDG id of the particle
		uint64_t part_id;  /// Parent particle id if info is known
		double vtx_x;
		double vtx_y;
		double vtx_z;
		double dir_x;   /// direction unit vector X at the vertex point
		double dir_y;   /// direction unit vector Y at the vertex point
		double dir_z;   /// direction unit vector Z at the vertex point
		double p;       /// Total momentum [gev]
		double px;      /// p * dir_x
		double py;      /// p * dir_y
		double pz;      /// p * dir_z
	};
}
#endif // MC_MINIMODEL_TRACK_HH

