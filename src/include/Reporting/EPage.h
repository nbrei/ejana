//
// Created by DmitryRa on 2/19/2019.
//

#ifndef EJANA_EPAGE_H
#define EJANA_EPAGE_H

#include <string>

namespace ej
namespace report
{

    class EPage
    {
    public:

    };

    class EPageElement
    {
    public:

    };

    class ENamedItem
    {
    public:
        std::string name;
    };

    class ERootItem:EPageItem
    {

    };


    ostream& operator<<(EPage& page, const Date& dt)
    {
        os << dt.mo << '/' << dt.da << '/' << dt.yr;
        return os;
    }

}

#endif //EJANA_EPAGE_H
